﻿using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace Conker.Console
{
    public static class HttpClientFactory
    {
        private static Task<string> GetToken()
        {
            // The AcquireTokenAsync call will prompt with a UI if necessary
            // Or otherwise silently use a refresh token to return
            // a valid access token	
            var token = Program.Token;

            return Task.FromResult(token);
        }

        public static HttpClient Create(string baseAddress) => new HttpClient(new AuthenticatedHttpClientHandler(GetToken))
        {
            BaseAddress = new Uri(baseAddress),
            Timeout = TimeSpan.FromSeconds(5),

        };
    }
}
