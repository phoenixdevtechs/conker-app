﻿using System.Threading;
using System.Threading.Tasks;
using Refit;

namespace Conker.Console
{
    public interface ILeaderBoardAPI
    {
        [Get("/api/leaderBoard")]
        [Headers("Authorization: Bearer")]
        Task<dynamic> GetAllLeaderBoards([Query] int fromRecords, [Query]  int pageSize, [Query]  string sort, [Query]  string filterByExample, CancellationToken cancellationToken = default(CancellationToken));

        [Get("/api/leaderBoard/{leaderBoardId}")]
        [Headers("Authorization")]
        Task<dynamic> GetOneLeaderBoard([AliasAs("leaderBoardId")] long leaderBoardId, CancellationToken cancellationToken = default(CancellationToken));

    }
}
