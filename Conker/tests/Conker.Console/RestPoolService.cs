﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Refit;

namespace Conker.Console
{

    public class RestPoolService : IRestPoolService
    {
        public ILeaderBoardAPI LeaderBoardAPI { get; private set; }

        public RestPoolService()
        {
            UpdateApiUrl("http://0a95ef9a.ngrok.io");
        }

        public void UpdateApiUrl(string newApiUrl)
        {
            var settings = new RefitSettings
            {
                ContentSerializer = new JsonContentSerializer(new JsonSerializerSettings
                {
                    ContractResolver  = new CamelCasePropertyNamesContractResolver()
                })
            };
            LeaderBoardAPI = RestService.For<ILeaderBoardAPI>(HttpClientFactory.Create(newApiUrl));
        }
    }

    public class AuthenticatedHttpClientHandler : HttpClientHandler
    {
        private readonly Func<Task<string>> getToken;

        public AuthenticatedHttpClientHandler(Func<Task<string>> getToken)
        {
            if (getToken == null) throw new ArgumentNullException(nameof(getToken));
            this.getToken = getToken;
        }

        protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            // See if the request has an authorize header
            var auth = request.Headers.Authorization;
            if (auth != null)
            {
                var token = await getToken().ConfigureAwait(false);
                request.Headers.Authorization = new AuthenticationHeaderValue(auth.Scheme, token);
            }

            return await base.SendAsync(request, cancellationToken).ConfigureAwait(false);
        }
    }
}
