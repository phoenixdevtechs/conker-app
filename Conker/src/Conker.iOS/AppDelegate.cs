﻿using Xamarin.Forms.Platform.iOS;
using Foundation;
using UIKit;
using UserNotifications;

namespace Conker.iOS
{
    [Register("AppDelegate")]
    public partial class AppDelegate : FormsApplicationDelegate
    {
        public override bool FinishedLaunching(UIApplication uiApplication, NSDictionary launchOptions)
        {


            if (UIDevice.CurrentDevice.CheckSystemVersion(10, 0))
            {
                // Ask the user for permission to get notifications on iOS 10.0+
                UNUserNotificationCenter.Current.RequestAuthorization(
                        UNAuthorizationOptions.Alert | UNAuthorizationOptions.Badge | UNAuthorizationOptions.Sound,
                        (approved, error) => { });

                // Watch for notifications while app is active
                UNUserNotificationCenter.Current.Delegate = new Framework.LocalNotifications.UserNotificationCenterDelegate();
            }
            else if (UIDevice.CurrentDevice.CheckSystemVersion(8, 0))
            {
                // Ask the user for permission to get notifications on iOS 8.0+
                var settings = UIUserNotificationSettings.GetSettingsForTypes(
                        UIUserNotificationType.Alert | UIUserNotificationType.Badge | UIUserNotificationType.Sound,
                        new NSSet());

                UIApplication.SharedApplication.RegisterUserNotificationSettings(settings);
            }


            SQLitePCL.Batteries_V2.Init();

            global::Xamarin.Forms.Forms.Init();
            Xamarin.Forms.FormsMaterial.Init();

            Effects.Effects.Init();

            XF.Material.iOS.Material.Init();
            Lottie.Forms.iOS.Renderers.AnimationViewRenderer.Init();
            ImageCircle.Forms.Plugin.iOS.ImageCircleRenderer.Init();
            global::Rg.Plugins.Popup.Popup.Init();
            global::FFImageLoading.Forms.Platform.CachedImageRenderer.Init();
            global::FFImageLoading.ImageService.Instance.Initialize();
            Plugin.InputKit.Platforms.iOS.Config.Init();
            // Code for starting up the Xamarin Test Cloud Agent


            LoadApplication(new App(new iOSInitializer()));


            return base.FinishedLaunching(uiApplication, launchOptions);
        }
    }
}
