﻿using Conker.iOS.Renderers;
using CoreGraphics;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

//to fix all frame's shadow
[assembly: ExportRenderer(typeof(Frame), typeof(LessShadowFrameRenderer))]
namespace Conker.iOS.Renderers
{
    public class LessShadowFrameRenderer : FrameRenderer
    {
        public override void Draw(CGRect rect)
        {
            base.Draw(rect);

            // Make shadow more acceptable
            Layer.ShadowRadius = 1.3f;
            Layer.ShadowColor = UIColor.Gray.CGColor;
            Layer.ShadowOffset = new CGSize(1, 1);
            Layer.ShadowOpacity = 0.80f;
            Layer.ShadowPath = UIBezierPath.FromRect(Layer.Bounds).CGPath;
            Layer.MasksToBounds = true;
        }
    }
}