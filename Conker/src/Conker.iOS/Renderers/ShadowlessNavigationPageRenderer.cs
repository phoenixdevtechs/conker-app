﻿using Conker.Controls;
using Conker.iOS.Renderers;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

//override directly NavigationPage
[assembly: ExportRenderer(typeof(CustomNavigationPage), typeof(ShadowlessNavigationPageRenderer))]
namespace Conker.iOS.Renderers
{
    public class ShadowlessNavigationPageRenderer : NavigationRenderer
    {
        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            // remove lower border and shadow of the navigation bar
            NavigationBar.SetBackgroundImage(new UIImage(), UIBarMetrics.Default);
            NavigationBar.ShadowImage = new UIImage();
        }
    }
}