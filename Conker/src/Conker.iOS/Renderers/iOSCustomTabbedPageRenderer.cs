﻿using Conker.Controls;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(CustomTabbedPage), typeof(Conker.iOS.Renderers.iOSCustomTabbedPageRenderer))]
namespace Conker.iOS.Renderers
{
    /// <summary>
    /// TODO: https://github.com/CrossGeeks/ExtendedTabbedPageSample/blob/master/CustomTabbedPage.iOS/ExtendedTabbedPageRenderer.cs
    /// </summary>
    public class iOSCustomTabbedPageRenderer : TabbedRenderer
    {
        protected override void OnElementChanged(VisualElementChangedEventArgs e)
        {
            base.OnElementChanged(e);
            Element.PropertyChanged += OnElementPropertyChanged;
        }

        private void OnElementPropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName.Equals(CustomTabbedPage.IsHiddenProperty.PropertyName))
            {
                if (Element is CustomTabbedPage customTabbed)
                {
                    TabBar.Hidden = customTabbed.IsHidden;
                }
            }
        }
    }
}