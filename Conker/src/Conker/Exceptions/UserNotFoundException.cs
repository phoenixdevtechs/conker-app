﻿using System;
using System.Runtime.Serialization;

namespace Conker.Exceptions
{
    public class UserNotFoundException :System.Exception
    {
        public UserNotFoundException()
       : base()
        { }

        public UserNotFoundException(string message)
            : base(message)

        { }

        public UserNotFoundException(string message, Exception innerException)
            : base(message, innerException)
        { }

        protected UserNotFoundException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        { }
    }
}
