﻿
namespace Conker.Services.Cache
{
    public static class CacheKey
    {
        public const string UserPhoto = "user_photo";
        public const string Teams = "teams";
    }
}
