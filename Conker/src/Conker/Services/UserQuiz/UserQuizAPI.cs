﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using Conker.Services.Common;
using Conker.Services.UserQuiz.DTOs;
using Flurl.Http;

namespace Conker.Services.UserQuiz
{
    public class UserQuizAPI : IUserQuizAPI
    {
        private readonly IFlurlClient _flurlClient;

        public UserQuizAPI(IFlurlClient flurlClient)
        {
            _flurlClient = flurlClient;

        }

        public Task<FinishStartedQuizResponseDto> FinishStartedQuiz(UserQuizQuestionRequestDto registerRequest)
        {
            var token = RestPoolService.GetToken();
                               
            return _flurlClient
                              // .Configure(settings => settings.BeforeCallAsync = BeforeCallAsync)
                               .Request("api")
                               .AppendPathSegment("userQuiz")
                               .AppendPathSegment("finishQuiz")
                               .WithOAuthBearerToken(token)
                               .PostJsonAsync(registerRequest)
                               .ReceiveJson<FinishStartedQuizResponseDto>();

                             
        }

        public Task<AllPublishedQuizResponseDto> GetAllPublishedQuiz(int fromRecords = 0, int pageSize = 20, string sort = "", string filterByExample = "")
        {
            var token = RestPoolService.GetToken();

            return _flurlClient.Request("api")
                               .AppendPathSegment("userQuiz")
                               .WithOAuthBearerToken(token)
                               .SetQueryParam(nameof(fromRecords), fromRecords)
                               .SetQueryParam(nameof(pageSize), pageSize)
                               .SetQueryParam(nameof(sort), sort)
                               .SetQueryParam(nameof(filterByExample), filterByExample)
                               .GetJsonAsync<AllPublishedQuizResponseDto>();
        }

        public Task<UserResultForASpecificLeaderBoardResponseDto> GetUserResultForASpecificLeaderBoard(long leaderBoardId, int fromRecords = 0, int pageSize = 20)
        {
            var token = RestPoolService.GetToken();

            return _flurlClient.Request("api")
                               .AppendPathSegment("userQuiz")
                               .AppendPathSegment("resultLeaderBoard")
                               .AppendPathSegment(leaderBoardId)
                               .WithOAuthBearerToken(token)
                               .GetJsonAsync<UserResultForASpecificLeaderBoardResponseDto>();
        }

        public Task<FinishStartedQuizResponseDto> GetUserResultForASpecificQuiz(long quizId)
        {
            var token = RestPoolService.GetToken();

            return _flurlClient.Request("api")
                               .AppendPathSegment("userQuiz")
                               .AppendPathSegment("result")
                               .AppendPathSegment(quizId)
                               .WithOAuthBearerToken(token)
                               .GetJsonAsync<FinishStartedQuizResponseDto>();
        }

        public Task<StartNewQuizResponseDto> StartNewQuiz(long quizId)
        {
            var token = RestPoolService.GetToken();

            return _flurlClient.Request("api")
                               .AppendPathSegment("userQuiz")
                               .AppendPathSegment("startQuiz")
                               .AppendPathSegment(quizId)
                               .WithOAuthBearerToken(token)
                               .PostStringAsync("")//TODO: Look at here wont work!
                               .ReceiveJson<StartNewQuizResponseDto>();
        }

        public Task<TodayQuizResponseDto> TodayQuiz()
        {
            var token = RestPoolService.GetToken();

            return _flurlClient.Request("api")
                               .AppendPathSegment("userQuiz")
                                .AppendPathSegment("todaysQuiz")
                               .WithOAuthBearerToken(token)
                               .GetJsonAsync<TodayQuizResponseDto>();
        }

        public Task<UserQuizSummaryResponseDto> UserQuizSummary(long quizId)
        {
            var token = RestPoolService.GetToken();

            return _flurlClient.Request("api")
                               .AppendPathSegment("userQuiz")
                               .AppendPathSegment("result")
                               .AppendPathSegment(quizId)
                               .WithOAuthBearerToken(token)
                               .GetJsonAsync<UserQuizSummaryResponseDto>();
        }

        private async Task BeforeCallAsync(HttpCall arg)
        {
            Debug.WriteLine($"# Request Url: {arg.Request.RequestUri}");
            Debug.WriteLine($"# Request Method: {arg.Request.Method.ToString()}");

            var content = await arg.Request.Content.ReadAsStringAsync();
            Debug.WriteLine($"# Request Content: {content}");

            Debug.WriteLine("Request:");
            Debug.WriteLine(arg.Request.ToString());
        }
    }
}
