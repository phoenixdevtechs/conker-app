﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Conker.Services.UserQuiz
{
    public interface IUserQuizAPI
    {
        Task<DTOs.AllPublishedQuizResponseDto> GetAllPublishedQuiz(int fromRecords = 0, int pageSize = 20, string sort = "", string filterByExample = "");

        Task<DTOs.FinishStartedQuizResponseDto> GetUserResultForASpecificQuiz(long quizId);

        Task<DTOs.FinishStartedQuizResponseDto> FinishStartedQuiz(DTOs.UserQuizQuestionRequestDto registerRequest);

        Task<DTOs.UserResultForASpecificLeaderBoardResponseDto> GetUserResultForASpecificLeaderBoard(long leaderBoardId, int fromRecords = 0, int pageSize = 20);

        Task<DTOs.StartNewQuizResponseDto> StartNewQuiz(long quizId);

        Task<DTOs.TodayQuizResponseDto> TodayQuiz();

        Task<DTOs.UserQuizSummaryResponseDto> UserQuizSummary(long quizId);
    }
}
