﻿using Conker.Helpers;
using Newtonsoft.Json;

namespace Conker.Services.UserQuiz
{
    public static class FakeGetTodaysPublishedQuizResponse
    {
        public static DTOs.TodayQuizResponseDto Fake()
        {
            return JsonConvert.DeserializeObject<DTOs.TodayQuizResponseDto>(
                   EmbeddedResourceHelper.Load("Conker.Services.UserQuiz.FakeGetTodaysPublishedQuizResponse.json"));
        }
    }
}
