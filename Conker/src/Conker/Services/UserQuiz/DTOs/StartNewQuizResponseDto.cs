﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Conker.Services.UserQuiz.DTOs
{
    public class StartNewQuizResponseDto
    {
        public StartNewQuizResponseDto()
        {
            Questions = new List<Question>();
        }

        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public int id { get; set; }
        public string LastModifiedBy { get; set; }
        public DateTime LastModifiedDate { get; set; }
        public List<Question> Questions { get; set; }
        public DateTime ScheduledDate { get; set; }
        public ConkerType Type { get; set; }
    }

   
    public class Question
    {
        [JsonProperty("answer1")]
        public string Answer1 { get; set; }
        [JsonProperty("answer2")]
        public string Answer2 { get; set; }
        [JsonProperty("answer3")]
        public string Answer3 { get; set; }

        [JsonProperty("rightAnswer")]
        public string RightAnswer { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }

        [JsonProperty("id")]
        public int Id { get; set; }
        public string LastModifiedBy { get; set; }
        public DateTime LastModifiedDate { get; set; }

        [JsonProperty("question")]
        public string QuestionString { get; set; }
        public ConkerType Type { get; set; }
    }
}
