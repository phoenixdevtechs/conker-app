﻿using Newtonsoft.Json;

namespace Conker.Services.UserQuiz.DTOs
{
    public class FinishStartedQuizResponseDto
    {
        [JsonProperty("dateCompleted")]
        public string DateCompleted { get; set; }
        [JsonProperty("dateStarted")]
        public string DateStarted { get; set; }
        [JsonProperty("typeDTO")]
        public TypeDTO TypeDTO { get; set; }
        [JsonProperty("score")]
        public int Score { get; set; }
        [JsonProperty("timeToComplete")]
        public int TimeToComplete { get; set; }
        [JsonProperty("user")]
        public string User { get; set; }
        [JsonProperty("userAnswersDTOList")]
        public UseranswersDto[] UserAnswers { get; set; }
    }

    public class TypeDTO
    {
        //public int CreatedBy { get; set; }
        // public DateTime CreatedDate { get; set; }

        [JsonProperty("id")]
        public int Id { get; set; }
        //public int LastModifiedBy { get; set; }
        // public DateTime LastModifiedDate { get; set; }
        [JsonProperty("type")]
        public string Type { get; set; }
    }

}
