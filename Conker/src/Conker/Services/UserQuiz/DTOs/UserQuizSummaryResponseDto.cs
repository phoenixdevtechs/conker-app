﻿using Newtonsoft.Json;

namespace Conker.Services.UserQuiz.DTOs
{

    public class UserQuizSummaryResponseDto
    {
        [JsonProperty("typeDTO")]
        public Typedto TypeDTO { get; set; }
        [JsonProperty("user")]
        public string User { get; set; }
        [JsonProperty("dateStarted")]
        public object DateStarted { get; set; }
        [JsonProperty("dateCompleted")]
        public string DateCompleted { get; set; }
        [JsonProperty("timeToComplete")]
        public int TimeToComplete { get; set; }
        [JsonProperty("score")]
        public int Score { get; set; }
        [JsonProperty("userAnswersDTOList")]
        public UseranswersDto[] UserAnswersDTOList { get; set; }
    }

    public class Typedto
    {
        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("type")]
        public string Type { get; set; }
    }

    public class UseranswersDto
    {
        [JsonProperty("question")]
        public string Question { get; set; }
        [JsonProperty("correctAnswer")]
        public string CorrectAnswer { get; set; }
        [JsonProperty("correct")]
        public bool Correct { get; set; }
    }

}
