﻿using System;
using System.Collections.Generic;

namespace Conker.Services.UserQuiz.DTOs
{
    public class AllPublishedQuizResponseDto
    {

        public AllPublishedQuizResponseDto()
        {
            Data = new List<Datum>();
        }

        public List<Datum> Data { get; set; }
        public int TotalPages { get; set; }
        public int TotalElements { get; set; }
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
    }
}

public class Datum
{
    public int Id { get; set; }
    public string CreatedBy { get; set; }
    public long CreatedDate { get; set; }
    public object LastModifiedBy { get; set; }
    public object LastModifiedDate { get; set; }
    public ConkerType Type { get; set; }
    public long ScheduledDate { get; set; }
    public bool UserTaken { get; set; }
}

public class ConkerType
{
    public int CreatedBy { get; set; }
    public DateTime CreatedDate { get; set; }
    public int Id { get; set; }
    public int LastModifiedBy { get; set; }
    public DateTime LastModifiedDate { get; set; }
    public string Type { get; set; }
}
