﻿
namespace Conker.Services.UserQuiz.DTOs
{
    public class UserResultForASpecificLeaderBoardResponseDto
    {
        public long LeaderBoardId { get; set; }
        public string LeaderBoardName { get; set; }
        public int QuizId { get; set; }
        public string QuizName { get; set; }
        public int Result { get; set; }
        public int TimeSecond { get; set; }
        public long UserId { get; set; }
        public string UserName { get; set; }
    }
}
