﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace Conker.Services.UserQuiz.DTOs
{
    public class TodayQuizResponseDto
    {
        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("questions")]
        public List<QuestionDto> Questions { get; set; }
        [JsonProperty("scheduledDate")]
        public string ScheduledDate { get; set; }
        public Type2Dto Type { get; set; }

    }

    public class TypeDto
    {
        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("type")]
        public string Type { get; set; }
    }

    public class QuestionDto
    {
        [JsonProperty("Answer1")]
        public string answer1 { get; set; }
        [JsonProperty("Answer2")]
        public string answer2 { get; set; }
        [JsonProperty("answer3")]
        public string Answer3 { get; set; }
        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("question")]
        public string Question { get; set; }
        [JsonProperty("rightAnswer")]
        public string RightAnswer { get; set; }

        public TypeDto type { get; set; }
    }

    public class Type2Dto
    {
        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("type")]
        public string Type { get; set; }
    }

    
}
