﻿using System;

namespace Conker.Services.UserQuiz.DTOs
{
    public class UserQuizQuestionResponseDto
    {
        public DateTime DateCompleted { get; set; }
        public DateTime DateStarted { get; set; }
        public QuizDto Quiz { get; set; }
        public int Score { get; set; }
        public int TimeToComplete { get; set; }
        public string User { get; set; }
    }

    public class QuizDto
    {
        public int CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public int id { get; set; }
        public int LastModifiedBy { get; set; }
        public DateTime LastModifiedDate { get; set; }
        public string Type { get; set; }
    }
}
