﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace Conker.Services.UserQuiz.DTOs
{
    public class UserQuizQuestionRequestDto
    {
        public UserQuizQuestionRequestDto()
        {
            this.Question = new List<UserQuizQuestionDto>();
        }

        [JsonProperty("quizId")]
        public long QuizId { get; set; }
        [JsonProperty("userQuizQuestionRequestDtoList")]
        public List<UserQuizQuestionDto> Question { get; set; }
    }

    public class UserQuizQuestionDto
    {
        [JsonProperty("questionId")]
        public long QuestionId { get; set; }
        [JsonProperty("givenAnswer")]
        public string GivenAnswer { get; set; }
    }

}
