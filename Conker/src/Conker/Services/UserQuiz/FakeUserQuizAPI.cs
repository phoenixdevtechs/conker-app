﻿using System.Threading.Tasks;
using Conker.Helpers;
using Conker.Services.UserQuiz.DTOs;

namespace Conker.Services.UserQuiz
{
    public class FakeUserQuizAPI : IUserQuizAPI
    {
        public Task<FinishStartedQuizResponseDto> FinishStartedQuiz(UserQuizQuestionRequestDto registerRequest)
        {
            throw new System.NotImplementedException();
        }


        public Task<DTOs.AllPublishedQuizResponseDto> GetAllPublishedQuiz(int fromRecords = 0, int pageSize = 20, string sort = "", string filterByExample = "") =>
           FakeNetwork.ReturnAsync(FakeGetAllPublishedQuizResponse.Fake(fromRecords, pageSize, sort, filterByExample));


        public Task<UserResultForASpecificLeaderBoardResponseDto> GetUserResultForASpecificLeaderBoard(long leaderBoardId, int fromRecords = 0, int pageSize = 20)
        {
            throw new System.NotImplementedException();
        }

        public Task<FinishStartedQuizResponseDto> GetUserResultForASpecificQuiz(long id)
        {
            throw new System.NotImplementedException();
        }

        public Task<StartNewQuizResponseDto> StartNewQuiz(long id)
        {
            throw new System.NotImplementedException();
        }

        public Task<TodayQuizResponseDto> TodayQuiz() => FakeNetwork.ReturnAsync(FakeGetTodaysPublishedQuizResponse.Fake());

        public Task<UserQuizSummaryResponseDto> UserQuizSummary(long quizId) => FakeNetwork.ReturnAsync(FakeUserQuizSummaryResponse.Fake(quizId));
    }
}
