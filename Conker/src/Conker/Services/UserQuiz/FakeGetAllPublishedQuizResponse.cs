﻿using Conker.Helpers;
using Newtonsoft.Json;

namespace Conker.Services.UserQuiz
{
   public static  class FakeGetAllPublishedQuizResponse
    {
        public static DTOs.AllPublishedQuizResponseDto Fake( int fromRecords, int pageSize,string sort, string filterByExample)
        {
            return JsonConvert.DeserializeObject<DTOs.AllPublishedQuizResponseDto>(
                   EmbeddedResourceHelper.Load("Conker.Services.UserQuiz.FakeGetAllPublishedQuizResponse.json"));
        }
    }
}
