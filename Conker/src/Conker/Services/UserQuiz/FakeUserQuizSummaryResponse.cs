﻿using Conker.Helpers;
using Newtonsoft.Json;

namespace Conker.Services.UserQuiz
{
    public static class FakeUserQuizSummaryResponse
    {
        public static DTOs.UserQuizSummaryResponseDto Fake(long quizId)
        {
            return JsonConvert.DeserializeObject<DTOs.UserQuizSummaryResponseDto>(
                   EmbeddedResourceHelper.Load("Conker.Services.UserQuiz.FakeUserQuizSummaryResponse.json"));
        }
    }
}
