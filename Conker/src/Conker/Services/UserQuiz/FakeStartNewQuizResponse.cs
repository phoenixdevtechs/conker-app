﻿using System;
using System.Collections.Generic;
using System.Text;
using Conker.Helpers;
using Newtonsoft.Json;

namespace Conker.Services.UserQuiz
{
    public static class FakeStartNewQuizResponse
    {
        public static DTOs.StartNewQuizResponseDto Fake(long quizId)
        {
            return JsonConvert.DeserializeObject<DTOs.StartNewQuizResponseDto>(
                   EmbeddedResourceHelper.Load("Conker.Services.UserQuiz.FakeStartNewQuizResponse.json"));
        }
    }
}
