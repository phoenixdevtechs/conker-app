﻿using System.Collections.Generic;
using Conker.Helpers;
using Newtonsoft.Json;

namespace Conker.Services.Analytics
{
    public static class FakeLeaderBoardStatisticsResponse
    {
        public static List<DTOs.LeaderBoardStatisticsResponseDto> Fake()
        {
            return JsonConvert.DeserializeObject<List<DTOs.LeaderBoardStatisticsResponseDto>>(
                   EmbeddedResourceHelper.Load("Conker.Services.Analytics.FakeLeaderBoardStatisticsResponse.json"));
        }
    }
}
