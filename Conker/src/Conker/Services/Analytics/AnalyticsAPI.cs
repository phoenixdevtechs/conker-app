﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Conker.Services.Analytics.DTOs;
using Flurl.Http;

namespace Conker.Services.Analytics
{
    public class AnalyticsAPI : IAnalyticsAPI
    {
        private readonly IFlurlClient _flurlClient;

        public AnalyticsAPI(IFlurlClient flurlClient)
        {
            _flurlClient = flurlClient;

        }

        public Task<List<DTOs.LeaderBoardStatisticsResponseDto>> GetLeaderBoardDailyStatistics()
        {
            var token = Common.RestPoolService.GetToken();

            return _flurlClient.Request("api")
                             .AppendPathSegment("analytics")
                             .AppendPathSegment("leaderboard")
                             .AppendPathSegment("daily")
                             .AppendPathSegment("all")
                             .WithOAuthBearerToken(token)
                             .GetJsonAsync<List<DTOs.LeaderBoardStatisticsResponseDto>>();
        }

        public Task<LeaderBoardStatisticsResponseDto> GetLeaderBoardDailyStatistics(long leaderBoardId)
        {
            var token = Common.RestPoolService.GetToken();

            return _flurlClient.Request("api")
                             .AppendPathSegment("analytics")
                             .AppendPathSegment("leaderboard")
                             .AppendPathSegment("daily")
                             .AppendPathSegment(leaderBoardId)
                             .WithOAuthBearerToken(token)
                             .GetJsonAsync<DTOs.LeaderBoardStatisticsResponseDto>();
        }

        public Task<List<DTOs.LeaderBoardStatisticsResponseDto>> GetLeaderBoardWeeklyStatistics()
        {
            var token = Common.RestPoolService.GetToken();

            return _flurlClient.Request("api")
                             .AppendPathSegment("analytics")
                             .AppendPathSegment("leaderboard")
                             .AppendPathSegment("weekly")
                             .AppendPathSegment("all")
                             .WithOAuthBearerToken(token)
                             .GetJsonAsync<List<DTOs.LeaderBoardStatisticsResponseDto>>();
        }

        public Task<SpecificUserDailyScoreResponseDto> GetSpecificUserDailyScore(string userId)
        {
            var token = Common.RestPoolService.GetToken();

            return _flurlClient.Request("api")
                            .AppendPathSegment("analytics")
                            .AppendPathSegment("user")
                            .AppendPathSegment("dailyScore")
                            .AppendPathSegment(userId)
                            .WithOAuthBearerToken(token)
                            .GetJsonAsync<SpecificUserDailyScoreResponseDto>();
        }

        public Task<SpecificUserWeeklyScoreResponseDto> GetSpecificUserWeeklyScore(string userId)
        {
            var token = Common.RestPoolService.GetToken();

            return _flurlClient.Request("api")
                            .AppendPathSegment("analytics")
                            .AppendPathSegment("user")
                            .AppendPathSegment("weeklyScore")
                            .AppendPathSegment(userId)
                            .WithOAuthBearerToken(token)
                            .GetJsonAsync<SpecificUserWeeklyScoreResponseDto>();
        }

        public Task<YesterdayWinnerResponseDto> GetYesterdayWinner()
        {
            var token = Common.RestPoolService.GetToken();

            return _flurlClient.Request("api")
                            .AppendPathSegment("analytics")
                            .AppendPathSegment("leaderboard")
                            .AppendPathSegment("yesterdaysWinner")
                            .WithOAuthBearerToken(token)
                            .GetJsonAsync<YesterdayWinnerResponseDto>();
        }
    }
}
