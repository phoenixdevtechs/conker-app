﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Conker.Helpers;
using Conker.Services.Analytics.DTOs;

namespace Conker.Services.Analytics
{
    public class FakeAnalyticsAPI : IAnalyticsAPI
    {
        public Task<List<DTOs.LeaderBoardStatisticsResponseDto>> GetLeaderBoardDailyStatistics() =>
           FakeNetwork.ReturnAsync(FakeLeaderBoardStatisticsResponse.Fake());

        public Task<LeaderBoardStatisticsResponseDto> GetLeaderBoardDailyStatistics(long leaderBoardId)
        {
            throw new System.NotImplementedException();
        }

        public Task<List<DTOs.LeaderBoardStatisticsResponseDto>> GetLeaderBoardWeeklyStatistics() =>
           FakeNetwork.ReturnAsync(FakeLeaderBoardStatisticsResponse.Fake());

        public Task<SpecificUserDailyScoreResponseDto> GetSpecificUserDailyScore(string userId)
        {
            throw new System.NotImplementedException();
        }

        public Task<SpecificUserWeeklyScoreResponseDto> GetSpecificUserWeeklyScore(string userId)
        {
            throw new System.NotImplementedException();
        }

        public Task<YesterdayWinnerResponseDto> GetYesterdayWinner()
        {
            throw new System.NotImplementedException();
        }
    }
}
