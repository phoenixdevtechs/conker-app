﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Conker.Services.Analytics
{
    public interface IAnalyticsAPI
    {
        /// <summary>
        /// Get daily statistics for all leaderboards
        /// </summary>
        /// <returns></returns>
        Task<List<DTOs.LeaderBoardStatisticsResponseDto>> GetLeaderBoardDailyStatistics();

        /// <summary>
        /// Get daily statistics for a specific leaderboard
        /// </summary>
        /// <returns></returns>
        Task<DTOs.LeaderBoardStatisticsResponseDto> GetLeaderBoardDailyStatistics(long leaderBoardId);

        /// <summary>
        /// Get weekly statistics for all leaderboards
        /// </summary>
        /// <returns></returns>
        Task<List<DTOs.LeaderBoardStatisticsResponseDto>> GetLeaderBoardWeeklyStatistics();

        /// <summary>
        /// Get daily score for a specific user
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        Task<DTOs.SpecificUserDailyScoreResponseDto> GetSpecificUserDailyScore(string userId);

        /// <summary>
        /// Get weekly score for a specific user
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        Task<DTOs.SpecificUserWeeklyScoreResponseDto> GetSpecificUserWeeklyScore(string userId);

        /// <summary>
        /// Get the leaderboard which had the highest score yesterday
        /// </summary>
        /// <returns></returns>
        Task<DTOs.YesterdayWinnerResponseDto> GetYesterdayWinner();
    }
}
