﻿using System;
using Newtonsoft.Json;

namespace Conker.Services.Analytics.DTOs
{
    public class YesterdayWinnerResponseDto
    {
        [JsonProperty("color")]
        public string Color { get; set; }
        [JsonProperty("createdBy")]
        public string CreatedBy { get; set; }
        [JsonProperty("createdDate")]
        public DateTime? CreatedDate { get; set; }
        [JsonProperty("id")]
        public int? Id { get; set; }
        [JsonProperty("lastModifiedBy")]
        public string LastModifiedBy { get; set; }
        [JsonProperty("lastModifiedDate")]
        public DateTime? LastModifiedDate { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
    }

}
