﻿using Newtonsoft.Json;

namespace Conker.Services.Analytics.DTOs
{
    public class LeaderBoardStatisticsResponseDto
    {
        [JsonProperty("leaderboardId")]
        public int LeaderBoardId { get; set; }
        [JsonProperty("leaderboardName")]
        public string LeaderBoardName { get; set; }

        [JsonProperty(Required = Required.Default,PropertyName ="date")]
        public string Date { get; set; }
        [JsonProperty(Required = Required.Default, PropertyName = "id")]
        public int Id { get; set; }
        [JsonProperty("score")]
        public double Score { get; set; }
        [JsonProperty(Required = Required.Default, PropertyName = "rank")]
        public int Rank { get; set; }
    }
}
