﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace Conker.Services.Analytics.DTOs
{
    public class SpecificUserWeeklyScoreResponseDto
    {
        public SpecificUserWeeklyScoreResponseDto()
        {
            this.AnalyticsUserWeeklyScoreBreakdownDTOS = new List<AnalyticsUserWeeklyScoreBreakdownDTO>();
        }
        public List<AnalyticsUserWeeklyScoreBreakdownDTO> AnalyticsUserWeeklyScoreBreakdownDTOS { get; set; }

        [JsonProperty("leaderboardName")]
        public string LeaderboardName { get; set; }
        [JsonProperty("weeklyRank")]
        public int WeeklyRank { get; set; }
        [JsonProperty("weeklyScore")]
        public double WeeklyScore { get; set; }

    }

    public class AnalyticsUserWeeklyScoreBreakdownDTO
    {
        [JsonProperty("day")]
        public string Day { get; set; }
        [JsonProperty("quizType")]
        public string QuizType { get; set; }
        [JsonProperty("userScore")]
        public double UserScore { get; set; }
    }
}
