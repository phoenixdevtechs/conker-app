﻿using Newtonsoft.Json;

namespace Conker.Services.Analytics.DTOs
{
    public  class SpecificUserDailyScoreResponseDto
    {
        [JsonProperty("leaderboardRank")]
        public int LeaderboardRank { get; set; }
        [JsonProperty("leaderboardName")]
        public string LeaderboardName { get; set; }
        [JsonProperty("score")]
        public double Score{ get; set; }
    }
}
