﻿using Conker.Services.Analytics;
using Conker.Services.Leaderboard;
using Conker.Services.UserLeaderBoard;
using Conker.Services.UserQuiz;

namespace Conker.Services.Common
{

    public class FakeRestPoolService : IRestPoolService
    {
        public Authentication.IAuthenticationAPI AuthenticationAPI { get; } = new Authentication.FakeAuthenticationAPI();

        public OTP.IOneTimePasswordAPI OneTimePasswordAPI { get; } = new OTP.FakeOneTimePasswordAPI();

        public ILeaderBoardAPI LeaderBoardAPI => throw new System.NotImplementedException();

        public IUserQuizAPI UserQuizAPI => throw new System.NotImplementedException();

        public IUserLeaderBoardAPI UserLeaderBoardAPI => throw new System.NotImplementedException();

        public IAnalyticsAPI AnalyticsAPI { get; } = new Analytics.FakeAnalyticsAPI();

        public void UpdateApiUrl(string newApiUrl)
        {
            // Intentionally blank
        }
    }
}
