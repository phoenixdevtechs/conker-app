﻿using Flurl.Http;
using Flurl.Http.Configuration;

namespace Conker.Services.Common
{
    public class RestPoolService : IRestPoolService
    {
        private readonly IFlurlClient _flurlClient;

        public Authentication.IAuthenticationAPI AuthenticationAPI { get; private set; }
        public OTP.IOneTimePasswordAPI OneTimePasswordAPI { get; private set; }
        public Leaderboard.ILeaderBoardAPI LeaderBoardAPI { get; private set; }
        public UserQuiz.IUserQuizAPI UserQuizAPI { get; private set; }
        public UserLeaderBoard.IUserLeaderBoardAPI UserLeaderBoardAPI { get; private set; }
        public Analytics.IAnalyticsAPI AnalyticsAPI { get; private set; }

        public RestPoolService(IFlurlClientFactory flurlClientFac)
        {
            _flurlClient = flurlClientFac.Get(Helpers.AppConstants.RootApiUrl);

            RegisterApis();
        }

        private void RegisterApis()
        {

            //_flurlClient.Settings = new ClientFlurlHttpSettings
            //{
            //    HttpClientFactory = new MyCustomHttpClientFactory()
            //};

            AuthenticationAPI = new Authentication.AuthenticationAPI(_flurlClient);
            OneTimePasswordAPI = new OTP.OneTimePasswordAPI(_flurlClient);
            LeaderBoardAPI = new Leaderboard.LeaderBoardAPI(_flurlClient);
            UserQuizAPI = new UserQuiz.UserQuizAPI(_flurlClient);
            UserLeaderBoardAPI = new UserLeaderBoard.UserLeaderBoardAPI(_flurlClient);
            AnalyticsAPI = new Analytics.AnalyticsAPI(_flurlClient);
        }

        public static string GetToken()
        {
            // The AcquireTokenAsync call will prompt with a UI if necessary
            // Or otherwise silently use a refresh token to return
            // a valid access token	
            var token = Helpers.Settings.AuthToken;

            //if (token.IsNullOrEmpty())
            //{
            //    var refresh = Helpers.AppConstants.RootApiUrl
            //                  .AppendPathSegment("auth")
            //                  .AppendPathSegment("refresh")
            //                  .PostJsonAsync(null)
            //                  .ReceiveJson<Authentication.DTOs.LoginResponseDto>()
            //                  .GetAwaiter()
            //                  .GetResult();
            //}
            return token;
        }
    }
}
