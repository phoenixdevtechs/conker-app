﻿
namespace Conker.Services.Common
{
    public interface IRestPoolService
    {
        Authentication.IAuthenticationAPI AuthenticationAPI { get; }
        OTP.IOneTimePasswordAPI OneTimePasswordAPI { get; }
        Leaderboard.ILeaderBoardAPI LeaderBoardAPI { get; }
        UserQuiz.IUserQuizAPI UserQuizAPI { get; }
        UserLeaderBoard.IUserLeaderBoardAPI UserLeaderBoardAPI { get; }
        Analytics.IAnalyticsAPI AnalyticsAPI { get; }

    }
}
