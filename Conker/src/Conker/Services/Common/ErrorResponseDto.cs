﻿using Newtonsoft.Json;

namespace Conker.Services.Common
{
    public class ErrorResponseDto
    {
        [JsonProperty("timestamp")]
        public long Timestamp { get; set; }
        [JsonProperty("status")]
        public int Status { get; set; }
        [JsonProperty("error")]
        public string Error { get; set; }
        [JsonProperty("exception")]
        public string Exception { get; set; }
        [JsonProperty("message")]
        public string Message { get; set; }
        [JsonProperty("path")]
        public string Path { get; set; }
    }

}
