﻿
namespace Conker.Services.Connectivity
{
    public interface IConnectivityService
    {
        bool IsThereInternet { get; }
    }
}
