﻿using Newtonsoft.Json;

namespace Conker.Services.OTP.DTOs
{

    public class VerifyOtpResponseDto
    {
        [JsonProperty("value")]
        public string Value { get; set; }
    }
}
