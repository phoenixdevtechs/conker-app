﻿using System.Threading.Tasks;
using Flurl.Http;

namespace Conker.Services.OTP
{
    public class OneTimePasswordAPI : IOneTimePasswordAPI
    {
        private readonly IFlurlClient _flurlClient;

        public OneTimePasswordAPI(IFlurlClient flurlClient)
        {
            _flurlClient = flurlClient;
        }


        public Task<string> Generate(string email)
        {
            return _flurlClient.Request("otp")
                               .AppendPathSegment("generateOtp")
                               .AppendPathSegment(email,true)
                               .GetStringAsync();
        }

        public Task<DTOs.VerifyOtpResponseDto> Verify(int otp, string email)
        {
            return _flurlClient.Request("otp")
                               .AppendPathSegment("verifyOtp")
                               .AppendPathSegment(otp)
                               .AppendPathSegment(email,true)
                               .GetJsonAsync<DTOs.VerifyOtpResponseDto>();
        }
    }
}
