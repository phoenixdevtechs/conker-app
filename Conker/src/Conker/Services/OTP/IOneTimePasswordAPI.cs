﻿using System.Threading.Tasks;
using Newtonsoft.Json.Linq;

namespace Conker.Services.OTP
{
    public interface IOneTimePasswordAPI
    {
        Task<string> Generate(string email);

        Task<DTOs.VerifyOtpResponseDto> Verify(int otp,string email);
    }
}
