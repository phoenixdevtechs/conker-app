﻿using System.Threading.Tasks;
using Newtonsoft.Json.Linq;

namespace Conker.Services.OTP
{
    public class FakeOneTimePasswordAPI : IOneTimePasswordAPI
    {

        public Task<string> Generate(string email)
        {
            return Task.FromResult<string>("514208");
        }

        public Task<DTOs.VerifyOtpResponseDto> Verify(int otp, string email)
        {
            string valid = "Entered Otp is valid";
            string notValid = "Not Valid: Entered Otp is NOT valid. Please Retry!";

            return Task.FromResult<DTOs.VerifyOtpResponseDto>(new DTOs.VerifyOtpResponseDto {
                Value = valid
            });
        }
    }
}
