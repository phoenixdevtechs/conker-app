﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Acr.UserDialogs;
using Xamarin.Forms;
using XF.Material.Forms;
using XF.Material.Forms.UI;
using XF.Material.Forms.UI.Dialogs;
using XF.Material.Forms.UI.Dialogs.Configurations;

namespace Conker.Services.Dialog
{
    public class DialogService : IDialogService
    {
        #region User Dialog Implement

        public void HideLoading()
        {
            UserDialogs.Instance.HideLoading();
        }

        public Task ShowValidationMessageAsync(List<string> errorList, string message = null, string title = null, string confirmingText = "Ok", string dismissiveText = "Cancel", MaterialAlertDialogConfiguration configuration = null)
        {
            var view = new ContentView
            {
                HorizontalOptions = LayoutOptions.FillAndExpand,
                VerticalOptions = LayoutOptions.FillAndExpand
            };

            var layout = new StackLayout
            {
                Spacing = 5,
                HorizontalOptions = LayoutOptions.CenterAndExpand,
                VerticalOptions = LayoutOptions.CenterAndExpand
            };

            view.Content = layout;

            int viewIndex = layout.Children.IndexOf(view);

            for (int i = viewIndex + 1; i < errorList.Count; i++)
            {
                // Add new label if none exists
                layout.Children.Insert(i, new Label
                {
                    Text = errorList[i],
                    FontSize = 14,
                    StyleId = view.Id.ToString(),
                    TextColor = Color.FromHex("#EE4444"),
                    LineBreakMode = LineBreakMode.WordWrap,
                    MaxLines = 3,
                    FontFamily = XF.Material.Forms.Material.GetResource<OnPlatform<string>>("AvenirFontFamily"),
                });
            }
            if (configuration == null)
            {
                configuration = new MaterialAlertDialogConfiguration
                {
                    BackgroundColor = Color.White,
                    ButtonAllCaps = false,
                    ButtonFontFamily = XF.Material.Forms.Material.GetResource<OnPlatform<string>>("AvenirFontFamily"),
                    CornerRadius = 8,
                    MessageFontFamily = XF.Material.Forms.Material.GetResource<OnPlatform<string>>("AvenirFontFamily"),
                    MessageTextColor = Color.FromHex("#34AF23"),
                    TitleTextColor = Color.FromHex("#34AF23"),
                    TitleFontFamily = XF.Material.Forms.Material.GetResource<OnPlatform<string>>("AvenirFontFamily"),
                    ScrimColor = Color.FromHex("#34AF23").MultiplyAlpha(0.32),
                    TintColor = Color.FromHex("#34AF23")
                };
            }

            return MaterialDialog.Instance.ShowCustomContentAsync(view,
                message,
                title,
                confirmingText,
                dismissiveText, configuration);
        }

        public Task ShowAlertAsync(string message, string title, string buttonLabel)
        {
            return UserDialogs.Instance.AlertAsync(message, title, buttonLabel);
        }

        public void ShowLoading(string title = null, MaskType? maskType = null)
        {
            UserDialogs.Instance.ShowLoading(title, maskType);
        }

        public Task<bool> ConfirmAsync(ConfirmConfig config, CancellationToken? cancelToken = default(CancellationToken?))
        {
            return UserDialogs.Instance.ConfirmAsync(config, cancelToken);
        }

        public Task<bool> ConfirmAsync(string message, string title = null, string okText = null, string cancelText = null, CancellationToken? cancelToken = default(CancellationToken?))
        {
            return UserDialogs.Instance.ConfirmAsync(message, title, okText, cancelText, cancelToken);
        }

        public void ShowNotifaciton(ToastNotificationTypeEnum type, string message, int delay = 2000)
        {
            var config = new ToastConfig(message);
            config.SetPosition(ToastPosition.Top);
            config.SetDuration(delay);
            config.SetMessageTextColor(System.Drawing.Color.White);

            switch (type)
            {
                case ToastNotificationTypeEnum.Error:
                    config.SetBackgroundColor(System.Drawing.Color.DarkRed);
                    break;
                case ToastNotificationTypeEnum.Success:
                    config.SetBackgroundColor(System.Drawing.Color.Green);
                    break;
                case ToastNotificationTypeEnum.Info:
                    config.SetBackgroundColor(System.Drawing.Color.Orange);
                    break;
                case ToastNotificationTypeEnum.Warning:
                    config.SetBackgroundColor(Color.FromHex("#0670b2"));
                    break;
            }

            UserDialogs.Instance.Toast(config);
        }

        public void Progress(string title, bool show, MaskType? maskType = null)
        {
            UserDialogs.Instance.Progress(title, null, null, show, maskType);
        }

        public async Task<PromptResult> PromptAsync(string message, string title, string okText, string cancelText, string placeHolder, InputType inputType)
        {
            var promptResult = await UserDialogs.Instance.PromptAsync(message, title, okText, cancelText, placeHolder, inputType);
            return promptResult;
        }



        #endregion


        #region Material Input Implement

        public Task ShowSuccess(string message = "Success", int timeoutMillis = MaterialSnackbar.DurationShort)
        {
            return MaterialDialog.Instance.SnackbarAsync(message: message,
                msDuration: timeoutMillis, new MaterialSnackbarConfiguration
                {
                    BackgroundColor = Color.FromHex("#258218"),
                    ButtonAllCaps = false,
                    ButtonFontFamily = Material.GetResource<OnPlatform<string>>("AvenirFontFamily"),
                    MessageFontFamily = Material.GetResource<OnPlatform<string>>("AvenirFontFamily"),
                    MessageTextColor = Color.White,
                    TintColor = Color.White,
                    Margin = new Thickness(10, 0, 10, 0)
                });

        }

        public Task<string> InputAsync(string title = null, string message = null, string inputText = null, string inputPlaceholder = "Enter input", string confirmingText = "Ok", string dismissiveText = "Cancel", MaterialTextFieldInputType inputType = MaterialTextFieldInputType.Default, MaterialInputDialogConfiguration configuration = null)
        {
            var config = new MaterialInputDialogConfiguration
            {
                BackgroundColor = Color.White,
                ButtonAllCaps = false,
                ButtonFontFamily = Material.GetResource<OnPlatform<string>>("AvenirFontFamily"),
                InputPlaceholderColor = Color.FromHex("#0670b2"),
                InputPlaceholderFontFamily = Material.GetResource<OnPlatform<string>>("AvenirFontFamily"),
                InputTextColor = Color.FromHex("#333333"),
                InputType = inputType,
                InputTextFontFamily = Material.GetResource<OnPlatform<string>>("AvenirFontFamily"),
                MessageFontFamily = Material.GetResource<OnPlatform<string>>("AvenirFontFamily"),
                MessageTextColor = Color.FromHex("#6D6D72"),
                TitleFontFamily = Material.GetResource<OnPlatform<string>>("AvenirFontFamily"),
                TitleTextColor = Color.FromHex("#333333"),
                TintColor = Color.FromHex("#0670b2"),
            };

            if (configuration != null)
            {
                config = configuration;
            }

            return MaterialDialog.Instance.InputAsync(title,
                 message,
                 inputText,
                 inputPlaceholder,
                 confirmingText,
                 dismissiveText,
                 config
              );
        }

        public Task AlertAsync(string message, string title, MaterialAlertDialogConfiguration configuration = null)
        {
            var config = new MaterialAlertDialogConfiguration
            {
                BackgroundColor = Color.White,
                ButtonAllCaps = false,
                ButtonFontFamily = Material.GetResource<OnPlatform<string>>("AvenirFontFamily"),
                MessageFontFamily = Material.GetResource<OnPlatform<string>>("AvenirFontFamily"),
                MessageTextColor = Color.FromHex("#6D6D72"),
                TitleFontFamily = Material.GetResource<OnPlatform<string>>("AvenirFontFamily"),
                TitleTextColor = Color.FromHex("#333333"),
                TintColor = Color.FromHex("#0670b2"),
                
            };

            if (configuration != null)
            {
                config = configuration;
            }

            return MaterialDialog.Instance.AlertAsync(message,
                title,
                config
            );
        }

        public Task<IMaterialModalPage> LoadingDialogAsync(string message = "Please waiting...", MaterialLoadingDialogConfiguration configuration = null)
        {
            var config = new MaterialLoadingDialogConfiguration
            {
                BackgroundColor = Color.White,
                MessageFontFamily = Material.GetResource<OnPlatform<string>>("AvenirFontFamily"),
                MessageTextColor = Color.FromHex("#6D6D72"),
                TintColor = Color.FromHex("#0670b2"),
            };

            if (configuration != null)
            {
                config = configuration;
            }

            return MaterialDialog.Instance.LoadingDialogAsync(message, config);
        }

        public Task<IMaterialModalPage> LoadingSnackbarAsync(string message = "Please waiting...", MaterialSnackbarConfiguration configuration = null)
        {
            var config = new MaterialSnackbarConfiguration
            {
                BackgroundColor = Color.FromHex("#0670b2"),
                MessageFontFamily = Material.GetResource<OnPlatform<string>>("AvenirFontFamily"),
                MessageTextColor = Color.FromHex("#6D6D72"),
                TintColor = Color.White,
                ButtonAllCaps = false,
                ButtonFontFamily = Material.GetResource<OnPlatform<string>>("AvenirFontFamily"),
            };

            if (configuration != null)
            {
                config = configuration;
            }

            return MaterialDialog.Instance.LoadingSnackbarAsync(message, config);
        }

        #endregion
    }
}
