﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Acr.UserDialogs;
using XF.Material.Forms.UI;
using XF.Material.Forms.UI.Dialogs;
using XF.Material.Forms.UI.Dialogs.Configurations;

namespace Conker.Services.Dialog
{
    public interface IDialogService
    {
        #region User Dialog

      
        Task ShowAlertAsync(string message, string title, string buttonLabel);

        // void ShowError(string message, int timeout=2000);

        void ShowLoading(string title = null, MaskType? maskType = default(MaskType?));

        void HideLoading();

     
        Task<bool> ConfirmAsync(ConfirmConfig config, CancellationToken? cancelToken = default(CancellationToken?));

        Task<bool> ConfirmAsync(string message, string title = null, string okText = null, string cancelText = null, CancellationToken? cancelToken = default(CancellationToken?));

        void ShowNotifaciton(ToastNotificationTypeEnum type, string message, int delay = 2000);

        void Progress(string title, bool show, MaskType? maskType);

        Task<PromptResult> PromptAsync(string message, string title, string okText, string cancelText, string placeHolder,
            InputType inputType);


        #endregion


        #region Material Input Interfaces
        Task ShowValidationMessageAsync(List<string> errorList, string message = null, string title = null, string confirmingText = "Ok", string dismissiveText = "Cancel", XF.Material.Forms.UI.Dialogs.Configurations.MaterialAlertDialogConfiguration configuration = null);

        Task<string> InputAsync(string title = null, string message = null, string inputText = null, string inputPlaceholder = "Enter input", string confirmingText = "Ok", string dismissiveText = "Cancel", MaterialTextFieldInputType inputType =MaterialTextFieldInputType.Default, MaterialInputDialogConfiguration configuration = null);
        Task ShowSuccess(string message = "Success", int timeoutMillis = MaterialSnackbar.DurationShort);
        Task AlertAsync(string message, string title, MaterialAlertDialogConfiguration configuration = null);

        Task<IMaterialModalPage> LoadingDialogAsync(string message = "Please waiting...", MaterialLoadingDialogConfiguration configuration = null);

        Task<IMaterialModalPage> LoadingSnackbarAsync(string message = "Please waiting...", MaterialSnackbarConfiguration configuration = null);

        #endregion

    }

    public enum ToastNotificationTypeEnum
    {
        Success,
        Warning,
        Error,
        Info
    }
}
