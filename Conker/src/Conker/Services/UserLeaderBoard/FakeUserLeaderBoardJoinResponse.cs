﻿using Conker.Helpers;
using Newtonsoft.Json;

namespace Conker.Services.UserLeaderBoard
{
    public static class FakeUserLeaderBoardJoinResponse
    {
        public static DTOs.UserLeaderBoardJoinResponseDto Fake(DTOs.UserLeaderBoardJoinRequestDto requestDto)
        {
            return JsonConvert.DeserializeObject<DTOs.UserLeaderBoardJoinResponseDto>(
                   EmbeddedResourceHelper.Load("Conker.Services.UserLeaderBoard.FakeUserLeaderBoardJoinResponse.json"));
        }
    }
}
