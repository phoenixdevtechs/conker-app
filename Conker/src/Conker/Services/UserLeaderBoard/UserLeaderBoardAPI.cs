﻿using System.Threading.Tasks;
using Conker.Services.UserLeaderBoard.DTOs;
using Flurl.Http;

namespace Conker.Services.UserLeaderBoard
{
    public class UserLeaderBoardAPI : IUserLeaderBoardAPI
    {
        private readonly IFlurlClient _flurlClient;

        public UserLeaderBoardAPI(IFlurlClient flurlClient)
        {
            _flurlClient = flurlClient;

        }

        public Task<UserLeaderBoardJoinResponseDto> Join(UserLeaderBoardJoinRequestDto requestDto)
        {
            var token = Common.RestPoolService.GetToken();

            return _flurlClient.Request("api")
                             .AppendPathSegment("userLeaderboard")
                             .AppendPathSegment("join")
                             .WithOAuthBearerToken(token)
                             .PostJsonAsync(requestDto)
                             .ReceiveJson<UserLeaderBoardJoinResponseDto>();
        }
    }
}
