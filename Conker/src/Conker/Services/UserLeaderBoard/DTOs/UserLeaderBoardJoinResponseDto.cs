﻿using System;
using Newtonsoft.Json;

namespace Conker.Services.UserLeaderBoard.DTOs
{
    public class UserLeaderBoardJoinResponseDto
    {
        [JsonProperty("id")]
        public int id { get; set; }
        [JsonProperty("leaderboardDTO")]
        public Leaderboarddto Leaderboard { get; set; }
        [JsonProperty("userDTO")]
        public Userdto User { get; set; }
    }


    public class Leaderboarddto
    {
        [JsonProperty("color")]
        public string Color { get; set; }
        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
    }

    public class Userdto
    {
        [JsonProperty("email")]
        public string Email { get; set; }
        [JsonProperty("enabled")]
        public bool Enabled { get; set; }
        [JsonProperty("firstName")]
        public string FirstName { get; set; }
        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("lastName")]
        public string LastName { get; set; }
        [JsonProperty("lastPasswordResetDate")]
        public string LastPasswordResetDate { get; set; }
        [JsonProperty("phoneNumber")]
        public string PhoneNumber { get; set; }
        [JsonProperty("username")]
        public string Username { get; set; }
    }

}
