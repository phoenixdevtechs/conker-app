﻿
using Newtonsoft.Json;

namespace Conker.Services.UserLeaderBoard.DTOs
{
    public class UserLeaderBoardJoinRequestDto
    {
        [JsonProperty(PropertyName = "leaderboardId")]
        public long LeaderBoardId { get; set; }
        [JsonProperty(PropertyName = "userId")]
        public long UserId { get; set; }
    }
}
