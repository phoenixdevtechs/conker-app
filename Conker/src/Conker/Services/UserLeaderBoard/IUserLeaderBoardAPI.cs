﻿using System.Threading.Tasks;
namespace Conker.Services.UserLeaderBoard
{
    public interface IUserLeaderBoardAPI
    {

        /// <summary>
        ///  post /api/userLeaderBoard/join
        ///  User can join a LeaderBoard
        /// </summary>
        /// <param name="requestDto"></param>
        /// <returns>UserLeaderBoardJoinResponseDto</returns>
        Task<DTOs.UserLeaderBoardJoinResponseDto> Join(DTOs.UserLeaderBoardJoinRequestDto registerRequest);
    }
}
