﻿using System.Threading;
using System.Threading.Tasks;
using Conker.Helpers;
using Conker.Services.UserLeaderBoard.DTOs;

namespace Conker.Services.UserLeaderBoard
{
    public class FakeUserLeaderBoardAPI : IUserLeaderBoardAPI
    {
        public async Task<UserLeaderBoardJoinResponseDto> Join(UserLeaderBoardJoinRequestDto registerRequest) => await FakeNetwork.ReturnAsync(FakeUserLeaderBoardJoinResponse.Fake(registerRequest));
    }
}
