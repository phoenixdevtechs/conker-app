﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace Conker.Services.Leaderboard.DTOs
{
    public class AllLeaderBoardsResponseDto
    {
        [JsonProperty("color")]
        public string Color { get; set; }
        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
        //public List<Datum> Data { get; set; }
        //public int TotalPages { get; set; }
        //public int TotalElements { get; set; }
        //public int PageNumber { get; set; }
        //public int PageSize { get; set; }
    }

    public class Datum
    {
        public long Id { get; set; }
        public object CreatedBy { get; set; }
        public object CreatedDate { get; set; }
        public object LastModifiedBy { get; set; }
        public object LastModifiedDate { get; set; }
        public string Name { get; set; }
        public string Color { get; set; } = "#0670b2";
    }

}
