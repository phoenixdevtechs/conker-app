﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Conker.Services.Leaderboard
{
    public interface ILeaderBoardAPI
    {
        Task<List<DTOs.AllLeaderBoardsResponseDto>> GetAllLeaderBoards(string userId = "");

        Task<object> GetOneLeaderBoard(long leaderBoardId);

    }
}
