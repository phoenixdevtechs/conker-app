﻿using System;
using System.Collections.Generic;
using System.Reactive.Linq;
using System.Threading.Tasks;
using Conker.Services.Cache;
using Conker.Services.Common;
using Conker.Services.Leaderboard.DTOs;
using Flurl.Http;

namespace Conker.Services.Leaderboard
{
    public class LeaderBoardAPI : ILeaderBoardAPI
    {
        private readonly IFlurlClient _flurlClient;

        public LeaderBoardAPI(IFlurlClient flurlClient)
        {
            _flurlClient = flurlClient;

        }

        public async Task<List<AllLeaderBoardsResponseDto>> GetAllLeaderBoards(string userId = "")
        {
            if (string.IsNullOrEmpty(Helpers.Settings.UserId))
                throw new Exceptions.UserNotFoundException(Resources.AppResources.UserNotFoundExceptionMessage);

            var cachedData = AkavacheImpl.GetAndFetchLatest($"current_user_{Helpers.Settings.UserId}",
          () => GetAllLeaderBoardsRemote(),
          offset =>
          {
              TimeSpan elapsed = DateTimeOffset.Now - offset;
              return elapsed > new TimeSpan(hours: 24, minutes: 0, seconds: 0);
          });

            return await cachedData.FirstOrDefaultAsync();
        }

        private Task<List<AllLeaderBoardsResponseDto>> GetAllLeaderBoardsRemote()
        {
            var token = RestPoolService.GetToken();


            return _flurlClient.Request("api")
                             .AppendPathSegment("leaderboard")
                             .WithOAuthBearerToken(token)
                            
                             .GetJsonAsync<List<AllLeaderBoardsResponseDto>>();
        }



        public Task<object> GetOneLeaderBoard(long leaderBoardId)
        {
            var token = RestPoolService.GetToken();

            return _flurlClient.Request("api")
                               .AppendPathSegment("leaderboard")
                               .AppendPathSegment(leaderBoardId)
                               .WithOAuthBearerToken(token)
                               .GetJsonAsync<object>();
        }



    }
}
