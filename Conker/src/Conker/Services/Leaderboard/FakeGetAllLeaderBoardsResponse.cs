﻿using System.Collections.Generic;
using Conker.Helpers;
using Newtonsoft.Json;

namespace Conker.Services.Leaderboard
{
    public static class FakeGetAllLeaderBoardsResponse
    {
        public static List<DTOs.AllLeaderBoardsResponseDto> Fake()
        {
          
            return JsonConvert.DeserializeObject<List<DTOs.AllLeaderBoardsResponseDto>>(
                   EmbeddedResourceHelper.Load("Conker.Services.Leaderboard.GetAllLeaderBoardsResponse.json"));
        }
    }
}
