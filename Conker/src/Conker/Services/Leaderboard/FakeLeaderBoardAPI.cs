﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Conker.Helpers;

namespace Conker.Services.Leaderboard
{
    public class FakeLeaderBoardAPI : ILeaderBoardAPI
    {
        public async Task<List<DTOs.AllLeaderBoardsResponseDto>> GetAllLeaderBoards(string userId="") =>
           await FakeNetwork.ReturnAsync(FakeGetAllLeaderBoardsResponse.Fake());

        public Task<object> GetOneLeaderBoard(long leaderBoardId)
        {
            throw new System.NotImplementedException();
        }
    }
}
