﻿using Conker.Helpers;
using Newtonsoft.Json;

namespace Conker.Services.Authentication
{
    public static class FakeRegisterResponse
    {
        public static DTOs.RegisterResponseDto Fake(DTOs.RegisterRequestDto registerRequest)
        {
            return JsonConvert.DeserializeObject<DTOs.RegisterResponseDto>(
                   EmbeddedResourceHelper.Load("Conker.Services.Authentication.FakeRegisterResponse.json"));
        }
    }
}
