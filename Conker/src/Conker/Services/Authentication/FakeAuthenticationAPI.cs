﻿using System.Threading.Tasks;
using Conker.Helpers;
using Conker.Services.Authentication.DTOs;

namespace Conker.Services.Authentication
{
    public class FakeAuthenticationAPI : IAuthenticationAPI
    {

        public Task<object> ChangePassword(DTOs.PasswordChangerDto requestDto)
        {
            throw new System.NotImplementedException();
        }

        public Task ForgotPassword(ForgotPasswordRequestDto requestDto)
        {
            throw new System.NotImplementedException();
        }

        public Task<CurrentUserResponseDto> GetCurrentUserAsync()
        {
            throw new System.NotImplementedException();
        }

        public async Task<DTOs.LoginResponseDto> Login(DTOs.AuthenticationRequestDto requestDto) =>
           await FakeNetwork.ReturnAsync(FakeLoginResponse.Fake(requestDto));

        public Task<LoginResponseDto> RefreshToken()
        {
            throw new System.NotImplementedException();
        }

        public async Task<DTOs.RegisterResponseDto> Register(DTOs.RegisterRequestDto registerRequest) =>
         await FakeNetwork.ReturnAsync(FakeRegisterResponse.Fake(registerRequest));

    }
}
