﻿using System.Threading.Tasks;

namespace Conker.Services.Authentication
{
    public interface IAuthenticationAPI
    {
        Task<DTOs.LoginResponseDto> Login(DTOs.AuthenticationRequestDto requestDto);

        Task<DTOs.RegisterResponseDto> Register(DTOs.RegisterRequestDto registerRequest);

        Task<object> ChangePassword(DTOs.PasswordChangerDto requestDto);

        Task<DTOs.LoginResponseDto> RefreshToken();

        Task<DTOs.CurrentUserResponseDto> GetCurrentUserAsync();

        Task ForgotPassword(DTOs.ForgotPasswordRequestDto requestDto);
    }
}
