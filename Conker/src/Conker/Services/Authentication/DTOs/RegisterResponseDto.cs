﻿
namespace Conker.Services.Authentication.DTOs
{
    public class RegisterResponseDto
    {
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Password { get; set; }
        public string Username { get; set; }
        public override string ToString()
        {
            return $"{FirstName} {LastName}";
        }
    }
}
