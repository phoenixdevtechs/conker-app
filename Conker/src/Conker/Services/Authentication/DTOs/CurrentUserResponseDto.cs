﻿using Newtonsoft.Json;

namespace Conker.Services.Authentication.DTOs
{
    public class CurrentUserResponseDto
    {
        //public Authority[] authorities { get; set; }
        //public int createdBy { get; set; }
        //public DateTime createdDate { get; set; }

        [JsonProperty("email")]
        public string Email { get; set; }
        //public bool enabled { get; set; }
        [JsonProperty("firstName")]
        public string FirstName { get; set; }

        [JsonProperty("id")]
        public long Id { get; set; }
        //public int lastModifiedBy { get; set; }
        //public DateTime lastModifiedDate { get; set; }
        [JsonProperty("lastName")]
        public string LastName { get; set; }
        //public Lastpasswordresetdate lastPasswordResetDate { get; set; }
        [JsonProperty("phoneNumber")]
        public string PhoneNumber { get; set; }
        [JsonProperty("username")]
        public string Username { get; set; }
        [JsonProperty("leaderboardName")]
        public string LeaderboardName { get; set; }
        [JsonProperty("leaderboardColour")]
        public string LeaderboardColour { get; set; }

    }

    public class Lastpasswordresetdate
    {
        public int date { get; set; }
        public int day { get; set; }
        public int hours { get; set; }
        public int minutes { get; set; }
        public int month { get; set; }
        public int nanos { get; set; }
        public int seconds { get; set; }
        public int time { get; set; }
        public int timezoneOffset { get; set; }
        public int year { get; set; }
    }

    public class Authority
    {
        public string authority { get; set; }
    }

}
