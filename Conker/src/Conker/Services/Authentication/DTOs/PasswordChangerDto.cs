﻿using Newtonsoft.Json;

namespace Conker.Services.Authentication.DTOs
{
    public class PasswordChangerDto
    {
        [JsonProperty("newPassword")]
        public string NewPassword { get; set; }
        [JsonProperty("oldPassword")]
        public string OldPassword { get; set; }
    }
}
