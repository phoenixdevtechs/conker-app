﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Conker.Services.Authentication.DTOs
{

    public class ForgotPasswordRequestDto
    {
        [JsonProperty("email")]
        public string Email { get; set; }
        [JsonProperty("password")]
        public string Password { get; set; }
        [JsonProperty("username")]
        public string Username { get; set; }
    }

}
