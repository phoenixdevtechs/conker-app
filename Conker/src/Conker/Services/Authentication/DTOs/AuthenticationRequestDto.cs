﻿
using Newtonsoft.Json;

namespace Conker.Services.Authentication.DTOs
{
    public class AuthenticationRequestDto
    {
        [JsonProperty("username")]
        public string Username { get; set; }
        [JsonProperty("password")]
        public string Password { get; set; }
    }
}
