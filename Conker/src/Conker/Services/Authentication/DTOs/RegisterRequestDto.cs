﻿using Newtonsoft.Json;

namespace Conker.Services.Authentication.DTOs
{
    public class RegisterRequestDto
    {
        [JsonProperty("email")]
        public string Email { get; set; }
        [JsonProperty("firstName")]
        public string FirstName { get; set; }
        [JsonProperty("lastName")]
        public string LastName { get; set; }
        [JsonProperty("password")]
        public string Password { get; set; }
        [JsonProperty("username")]
        public string Username { get; set; }
        [JsonProperty("phoneNumber")]
        public string PhoneNumber { get; set; }
        public override string ToString()
        {
            return $"{FirstName} {LastName}";
        }
    }
}
