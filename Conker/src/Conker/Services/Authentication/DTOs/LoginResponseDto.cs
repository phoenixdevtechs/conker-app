﻿using Newtonsoft.Json;

namespace Conker.Services.Authentication.DTOs
{
    public class LoginResponseDto
    {
        [JsonProperty(PropertyName = "access_token")]
        public string AccessToken { get; set; }

        [JsonProperty(PropertyName = "expires_in")]
        public int Expires { get; set; }
    }
}
