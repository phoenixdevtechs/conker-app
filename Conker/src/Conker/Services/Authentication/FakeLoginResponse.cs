﻿using Conker.Helpers;
using Newtonsoft.Json;

namespace Conker.Services.Authentication
{
    public static class FakeLoginResponse
    {
        public static DTOs.LoginResponseDto Fake(DTOs.AuthenticationRequestDto loginRequest)
        {
            if (loginRequest.Username != "appadmin" || loginRequest.Password != "app123")
            {
                throw new System.Exception("Wrong credentials!");
            }

            return JsonConvert.DeserializeObject<DTOs.LoginResponseDto>(
                   EmbeddedResourceHelper.Load("Conker.Services.Authentication.FakeLoginResponse.json"));
        }
    }
}
