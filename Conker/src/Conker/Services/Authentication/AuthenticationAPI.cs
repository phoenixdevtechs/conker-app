﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using Conker.Services.Authentication.DTOs;
using Flurl.Http;

namespace Conker.Services.Authentication
{
    public class AuthenticationAPI : IAuthenticationAPI
    {
        private readonly IFlurlClient _flurlClient;

        public AuthenticationAPI(IFlurlClient flurlClient)
        {
            _flurlClient = flurlClient;

        }

        public Task<object> ChangePassword(DTOs.PasswordChangerDto requestDto)
        {
            var token = Common.RestPoolService.GetToken();

            return _flurlClient.Request("auth")
                             .AppendPathSegment("change-password")
                             .WithOAuthBearerToken(token)
                             .PostJsonAsync(new
                             {
                                 newPassword = requestDto.NewPassword,
                                 oldPassword = requestDto.OldPassword
                             })
                             .ReceiveJson<object>();
        }

        public async Task ForgotPassword(ForgotPasswordRequestDto requestDto)
        {
            HttpResponseMessage response =await _flurlClient.Request("auth")
                              .AppendPathSegment("forgotPassword")
                              .PostJsonAsync(requestDto);

            return;
        }

        public Task<CurrentUserResponseDto> GetCurrentUserAsync()
        {
            var token = Common.RestPoolService.GetToken();

            return _flurlClient.Request("api")
                                    .AppendPathSegment("user")
                                    .WithOAuthBearerToken(token)
                                    .GetJsonAsync<CurrentUserResponseDto>();
        }


        public Task<LoginResponseDto> Login(AuthenticationRequestDto requestDto)
        {
            return _flurlClient.Request("auth")
                               .AppendPathSegment("login")
                               .PostJsonAsync(new
                               {
                                   username = requestDto.Username,
                                   password = requestDto.Password
                               })
                               .ReceiveJson<LoginResponseDto>();
        }

        public Task<LoginResponseDto> RefreshToken()
        {
            throw new NotImplementedException();
        }

        public Task<RegisterResponseDto> Register(RegisterRequestDto registerRequest)
        {
            return _flurlClient.Request("auth")
                               .AppendPathSegment("register")
                               .PostJsonAsync(new
                               {
                                   email = registerRequest.Email,
                                   firstName = registerRequest.FirstName,
                                   lastName = registerRequest.LastName,
                                   password = registerRequest.Password,
                                   username = registerRequest.Username,
                                   phoneNumber = registerRequest.PhoneNumber,
                               })
                               .ReceiveJson<RegisterResponseDto>();
        }
    }
}
