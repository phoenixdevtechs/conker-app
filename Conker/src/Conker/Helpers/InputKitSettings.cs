﻿using System;
using System.Collections.Generic;
using System.Text;
using Plugin.InputKit.Shared.Controls;
using Xamarin.Forms;

namespace Conker.Helpers
{
    public static class InputKitSettings
    {
        public static void Init()
        {
            SelectionView.GlobalSetting.FontFamily = "AvenirFontFamily"; //TODO: Get this from Prism Resources
            SelectionView.GlobalSetting.BorderColor = Color.Transparent;
        }
    }
}
