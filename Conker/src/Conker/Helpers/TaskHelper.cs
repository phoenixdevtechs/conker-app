﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using DryIoc;
using Conker.Logging;
using Conker.Services.Connectivity;
using Conker.Services.Dialog;
using OperationResult;
using Prism.Ioc;
using static OperationResult.Helpers;
using Conker.Extensions;
using Flurl.Http;
using Newtonsoft.Json;
using System.Net;

namespace Conker.Helpers
{
    public class TaskHelper
    {
        private readonly IConnectivityService connectivityService;
        private readonly IDialogService dialogService;
        private readonly ILoggingService loggingService;

        private Action whenStarting;
        private Action whenFinished;

        private TaskHelper()
        {
            loggingService = Prism.PrismApplicationBase.Current.Container.Resolve<ILoggingService>();
            connectivityService = Prism.PrismApplicationBase.Current.Container.Resolve<IConnectivityService>();
            dialogService = App.Current.Container.Resolve<IDialogService>();
        }

        public static TaskHelper Create()
        {
            return new TaskHelper();
        }

        public TaskHelper WhenStarting(Action action)
        {
            whenStarting = action;

            //dialogService.ShowLoading(Resources.AppResources.ShowLoadingTitle, Acr.UserDialogs.MaskType.Black);

            return this;
        }

        public TaskHelper WhenFinished(Action action)
        {
            whenFinished = action;
            //dialogService.HideLoading();
            return this;
        }

        public async Task<Status> TryWithErrorHandlingAsync(
            Task task,
            Func<Exception, Task<bool>> customErrorHandler = null)
        {
            var taskWrapper = new Func<Task<object>>(() => WrapTaskAsync(task));
            var result = await TryWithErrorHandlingAsync(taskWrapper(), customErrorHandler);

            if (result)
            {
                return Ok();
            }

            return Error();
        }

        public async Task<Result<T>> TryWithErrorHandlingAsync<T>(
            Task<T> task,
            Func<Exception, Task<bool>> customErrorHandler = null)
        {
            whenStarting?.Invoke();

            if (!connectivityService.IsThereInternet)
            {
                loggingService?.Warning("There's no Internet access");
                return Error();
            }

            try
            {
                T actualResult = await task;
                return Ok(actualResult);
            }
            catch (AggregateException ex)
            {
                foreach (Exception inner in ex.InnerExceptions)
                {
                    Console.WriteLine("Exception type {0} from {1}",
                        inner.GetType(), inner.Source);
                }
            }
            catch (FlurlHttpException exception)
            {
                loggingService?.Warning($"FlurlHttpException: {exception}");

                if (exception.Call.HttpStatus != null && exception.Call.HttpStatus == HttpStatusCode.NotFound)
                {
                    dialogService.HideLoading();

                    //await dialogService.ShowAlertAsync(
                    //"Server not found!",
                    //"Error",
                    //Resources.AppResources.AlertOKEllipsis);

                    return Error();
                }

                try
                {
                    var error = await exception.GetResponseJsonAsync<Services.Common.ErrorResponseDto>();

                    if (error != null)
                    {
                        dialogService.ShowNotifaciton(ToastNotificationTypeEnum.Warning,error.Message);

                        return Error();
                    }
                }
                catch
                {

                }
            }
            catch (TaskCanceledException exception)
            {
                loggingService?.Debug($"{exception}");
            }
            catch (JsonReaderException exception)
            {
                loggingService?.Debug($"{exception}");
            }
            catch (HttpRequestException exception)
            {
                loggingService?.Warning($"{exception}");

                if (customErrorHandler == null || !await customErrorHandler?.Invoke(exception))
                {
                    dialogService.HideLoading();

                    await dialogService.ShowAlertAsync(
                         Resources.AppResources.AlertMessageInternetError,
                         Resources.AppResources.AlertTitleUnexpectedError,
                         Resources.AppResources.AlertOKEllipsis);
                }
            }
            catch (Exception exception)
            {
                loggingService?.Error(exception);

                if (customErrorHandler == null || !await customErrorHandler?.Invoke(exception))
                {
                    dialogService.HideLoading();
                    await dialogService.ShowAlertAsync(
                         Resources.AppResources.AlertMessageInternetError,
                         Resources.AppResources.AlertTitleUnexpectedError,
                         Resources.AppResources.AlertOKEllipsis);
                }
            }
            finally
            {
                whenFinished?.Invoke();
            }

            return Error();
        }

        private async Task<object> WrapTaskAsync(Task innerTask)
        {
            await innerTask;

            return new object();
        }
    }
}
