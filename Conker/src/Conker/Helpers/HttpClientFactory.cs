﻿using System;
using System.Diagnostics;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;

namespace Conker.Helpers
{
    public static class HttpClientFactory
    {
        private static async Task<string> GetToken()
        {
            // The AcquireTokenAsync call will prompt with a UI if necessary
            // Or otherwise silently use a refresh token to return
            // a valid access token	
            var token = ""; //await Xamarin.Essentials.SecureStorage.GetAsync(Services.SecureStorage.StorageKey.Token).ConfigureAwait(false);

            return token;
        }

        public static HttpClient Create(string baseAddress) => new HttpClient(new AuthenticatedHttpClientHandler(GetToken))
        {
            BaseAddress = new Uri(baseAddress),
            Timeout = TimeSpan.FromSeconds(5),

        };
    }

    public class AuthenticatedHttpClientHandler : HttpClientHandler
    {
        private readonly Func<Task<string>> getToken;

        public AuthenticatedHttpClientHandler(Func<Task<string>> getToken)
        {
            if (getToken == null) throw new ArgumentNullException(nameof(getToken));
            this.getToken = getToken;
        }

        protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {

            // See if the request has an authorize header
            var auth = request.Headers.Authorization;
            if (auth != null)
            {
                var token = await getToken().ConfigureAwait(false);
                request.Headers.Authorization = new AuthenticationHeaderValue(auth.Scheme, token);
            }

#if DEBUG
            var totalElapsedTime = Stopwatch.StartNew();

            Debug.WriteLine($"Request: {request}");
            if (request.Content != null)
            {
                var content = await request.Content.ReadAsStringAsync().ConfigureAwait(false);
                Debug.WriteLine($"Request Content: {content}");
            }

            var responseElapsedTime = Stopwatch.StartNew();
            var response = await base.SendAsync(request, cancellationToken).ConfigureAwait(false);

            Debug.WriteLine($"Response: {response}");
            if (response.Content != null)
            {
                var content = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                Debug.WriteLine($"Response Content: {content}");
            }

            responseElapsedTime.Stop();
            Debug.WriteLine($"Response elapsed time: {responseElapsedTime.ElapsedMilliseconds} ms");

            totalElapsedTime.Stop();
            Debug.WriteLine($"Total elapsed time: {totalElapsedTime.ElapsedMilliseconds} ms");

            return response;
#else
            return await base.SendAsync(request, cancellationToken).ConfigureAwait(false);
#endif

        }
    }
}
