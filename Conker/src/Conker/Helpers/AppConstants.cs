﻿using System.Collections.Generic;
using Xamarin.Essentials;

namespace Conker.Helpers
{
  public static class AppConstants
  {
        // Put constants here that are not of a sensitive nature
        public static string IPAddress = DeviceInfo.Platform == DevicePlatform.Android ? "10.0.2.2" : "localhost";
        public static string BackendUrl = $"http://{IPAddress}:5000";
        public const string RootApiUrl = "http://65291d18.ngrok.io"; //"http://ec2-3-8-238-248.eu-west-2.compute.amazonaws.com:8080";
        public const string ApplicationName = "Conker";
        public const string ApiAuthorizationHeader = "Authorization: Bearer";
        public const bool BreakNetworkRandomly = !UITestMode && DebugMode;
        public const bool AndroidDebuggable = DebugMode;
        public const bool UseDebugLogging = UITestMode || DebugMode;
        public const bool UseFakeAPIs = UITestMode || DebugMode;
        public const bool UseFakeAuthentication = UITestMode || DebugMode;
        
        //App Center
        public const string AppCenterAndroidSecret = "def45b61-ca9f-4219-80ea-d46e1b64bae1";
        public const string AppCenteriOSSecret = "49310d54-0065-4737-a4f0-3369c27a1c83";


        public static Dictionary<string, string> LondonUnderground => new Dictionary<string, string>
        {
            {"Northern", "#000000"},
            {"Bakerloo", "#B26300"},
            {"Victoria", "#0098D8"},
            {"Circle", "#FFD329"},
            {"Central", "#DC241F"},
            {"Jubilee", "#A1A5A7"},
            {"Piccadilly", "#0019A8"},
            {"District", "#007D32"},
            {"Metropolitan", "#9B0058"},
            {"Hammersmith & City", "#F4A9BE"},
            {"Waterloo & City", "#93CEBA"},
            {"DLR", "#00AFAD"},
            {"Overground", "#00AFAD"},//TODO: What is color
            {"TfL Rail", "#0019A8"},
            {"London Trams", "#00BD19"},
        };


        public const bool UITestMode =
#if IS_UI_TEST
            true;
#else
            false;
#endif

        public const bool DebugMode =
#if DEBUG
            true;
#else
            false;
#endif
    }
}
