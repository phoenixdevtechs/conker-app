﻿using Xamarin.Forms;

namespace Conker.Helpers
{
  public static class NavigationPageHelper
  {
    public static NavigationPage Create(Page page)
    {
      return new NavigationPage(page) { BarTextColor = Color.White };
    }
  }
}
