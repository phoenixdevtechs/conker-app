﻿using Plugin.Settings;
using Plugin.Settings.Abstractions;

namespace Conker.Helpers
{
    public static class Settings
    {
        private static ISettings AppSettings => CrossSettings.Current;


        public static string AuthToken
        {
            get => AppSettings.GetValueOrDefault("auth_token_key", "");
            set => AppSettings.AddOrUpdateValue("auth_token_key", value);
        }

        public static string UserId
        {
            get => AppSettings.GetValueOrDefault("user_id_key", "");
            set => AppSettings.AddOrUpdateValue("user_id_key", value);
        }

        public static string Password
        {
            get => AppSettings.GetValueOrDefault("password_key", "");
            set => AppSettings.AddOrUpdateValue("password_key", value);
        }

    }
}
