﻿using System.Net.Http;
using Flurl.Http.Configuration;

namespace Conker.Helpers
{
    public class MyCustomHttpClientFactory : DefaultHttpClientFactory
    {
        // override to customize how HttpClient is created/configured
        public override HttpClient CreateHttpClient(HttpMessageHandler handler)
        {
            return base.CreateHttpClient(handler);  
        }

        // override to customize how HttpMessageHandler is created/configured
        public override HttpMessageHandler CreateMessageHandler()
        {
            return base.CreateMessageHandler(); 
        }

    }
}
