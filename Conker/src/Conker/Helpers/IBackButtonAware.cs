﻿
namespace Conker.Helpers
{
    public interface IBackButtonAware
    {
        bool OnBackButtonPressed();
    }
}
