﻿using System;
using Xamarin.Forms;

[assembly: ResolutionGroupName("Conker.Effects")]
namespace Conker.Effects
{
    /// <summary>
    /// Effect ids of all the available effects.
    /// Can be used to resolve an effect from code.
    ///     e.g. MyEntry.Effects.Add(Effect.Resolve(EffectIds.ViewBlurEffect));
    /// </summary>
    public class EffectIds
    {

        /// <summary>
        /// Id for <see cref="EntryRemoveLine"/>
        /// </summary>
        public static string EntryRemoveLine => typeof(EntryRemoveLine).FullName;

    }
}
