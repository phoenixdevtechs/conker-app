﻿using Xamarin.Forms;

namespace Conker.Effects
{
    public class NoShiftEffect : RoutingEffect
    {
        public NoShiftEffect() : base("Conker.NoShiftEffect")
        {

        }
    }
}
