﻿using System;
using Xamarin.Forms;

namespace Conker.Effects
{
    public class EntryRemoveLine : RoutingEffect
    {

        public EntryRemoveLine()
            : base(EffectIds.EntryRemoveLine)
        {
        }
    }
}
