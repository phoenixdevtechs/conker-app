using System.Threading.Tasks;
using Conker.Controls;
using Conker.Resources;
using Conker.Services;
using Conker.Views;
using DryIoc;
using Plugin.Multilingual;
using Prism;
using Prism.Ioc;
using Prism.Logging;
using Prism.Plugin.Popups;
using Xamarin.Forms;
using Xamarin.Forms.Internals;
using Conker.ViewModels;
using XF.Material.Forms.UI;
using DLToolkit.Forms.Controls;
using Conker.Helpers;

namespace Conker
{
    public partial class App
    {
        /* 
         * NOTE: 
         * The Xamarin Forms XAML Previewer in Visual Studio uses System.Activator.CreateInstance.
         * This imposes a limitation in which the App class must have a default constructor. 
         * App(IPlatformInitializer initializer = null) cannot be handled by the Activator.
         */
        public App()
            : this(null)
        {
        }

        public App(IPlatformInitializer initializer)
            : this(initializer, false)
        {
        }

        public App(IPlatformInitializer initializer, bool setFormsDependencyResolver)
            : base(initializer, setFormsDependencyResolver)
        {
        }

        protected override async void OnInitialized()
        {
            InitializeComponent();

            Plugin.InputKit.Shared.Controls.SelectionView.GlobalSetting.CornerRadius = 10;
            Plugin.InputKit.Shared.Controls.SelectionView.GlobalSetting.TextColor = Color.White;
            Plugin.InputKit.Shared.Controls.SelectionView.GlobalSetting.FontFamily = XF.Material.Forms.Material.GetResource<OnPlatform<string>>("AvenirBoldFontFamily");


       AppResources.Culture = CrossMultilingual.Current.DeviceCultureInfo;

#if DEBUG
            // Handle Xamarin Form Logging events such as Binding Errors
            Log.Listeners.Add(new TraceLogListener());
#endif
            InitAkavache();

            XF.Material.Forms.Material.Init(this);
            FlowListView.Init();
            InputKitSettings.Init();

            LogUnobservedTaskExceptions();
            AppResources.Culture = CrossMultilingual.Current.DeviceCultureInfo;

            await NavigationService.NavigateAsync(Screens.SplashScreenPage);
        }

        protected override void RegisterTypes(IContainerRegistry containerRegistry)
        {
            if (Helpers.AppConstants.UseDebugLogging)
            {
                containerRegistry.Register<Logging.ILoggingService, Logging.DebugLoggingService>();
            }
            else
            {
                containerRegistry.Register<Logging.ILoggingService, Logging.AppCenterLoggingService>();
            }


            containerRegistry.RegisterSingleton<Flurl.Http.Configuration.IFlurlClientFactory, Flurl.Http.Configuration.PerBaseUrlFlurlClientFactory>();


            //if (!Helpers.AppConstants.UseFakeAPIs)
            //{
            //    containerRegistry.RegisterSingleton<Services.Common.IRestPoolService, Services.Common.FakeRestPoolService>();
            //}
            //else
            //{
            containerRegistry.RegisterSingleton<Services.Common.IRestPoolService, Services.Common.RestPoolService>();
            //}

            containerRegistry.Register<Services.Connectivity.IConnectivityService, Services.Connectivity.ConnectivityService>();

            containerRegistry.RegisterSingleton<Services.Dialog.IDialogService, Services.Dialog.DialogService>();
            containerRegistry.RegisterInstance<Prism.Events.IEventAggregator>(new Prism.Events.EventAggregator());


            // Register the Popup Plugin Navigation Service
            containerRegistry.RegisterPopupNavigationService();


            // Navigating to "TabbedPage?createTab=ViewA&createTab=ViewB&createTab=ViewC will generate a TabbedPage
            // with three tabs for ViewA, ViewB, & ViewC
            // Adding `selectedTab=ViewB` will set the current tab to ViewB
            containerRegistry.RegisterForNavigation<CustomTabbedPage>(nameof(TabbedPage));
            containerRegistry.RegisterForNavigation<CustomNavigationPage>(nameof(NavigationPage));
            containerRegistry.RegisterForNavigation<MaterialNavigationPage>();
            containerRegistry.RegisterForNavigation<MainPage>();
            containerRegistry.RegisterForNavigation<SplashScreenPage>();
            containerRegistry.RegisterForNavigation<LoginPage>();
            containerRegistry.RegisterForNavigation<SettingsPage>();
            containerRegistry.RegisterForNavigation<SignUpPage>();
            containerRegistry.RegisterForNavigation<VerificationPage>();
            containerRegistry.RegisterForNavigation<PhotoChoosePage>();
            containerRegistry.RegisterForNavigation<TeamSelectionPage>();
            containerRegistry.RegisterForNavigation<MainTabbedPage>();
            containerRegistry.RegisterForNavigation<HomePage>();
            containerRegistry.RegisterForNavigation<LeagueTablePage>();
            containerRegistry.RegisterForNavigation<QuizPage>();
            containerRegistry.RegisterForNavigation<QuestionPage>();
            containerRegistry.RegisterForNavigation<PostPopupPage, PostPopupPageViewModel>();
            containerRegistry.RegisterForNavigation<ForgotPasswordPage>();
            containerRegistry.RegisterForNavigation<ChangePasswordPage>();
        }

        protected override void OnStart()
        {
            // Handle when your app starts
            Microsoft.AppCenter.AppCenter.Start($"android={Helpers.AppConstants.AppCenterAndroidSecret}; ios={Helpers.AppConstants.AppCenteriOSSecret}",
               typeof(Microsoft.AppCenter.Analytics.Analytics), typeof(Microsoft.AppCenter.Crashes.Crashes));
        }

        protected override void OnSleep()
        {
            // Handle IApplicationLifecycle
            base.OnSleep();

            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle IApplicationLifecycle
            base.OnResume();

            // Handle when your app resumes
        }

        private void LogUnobservedTaskExceptions()
        {
            TaskScheduler.UnobservedTaskException += (sender, e) =>
            {
                Container.Resolve<ILoggerFacade>().Log(e.Exception.ToString(), Category.Exception, Priority.None);
            };
        }

        private void InitAkavache()
        {
            Akavache.BlobCache.ApplicationName = Helpers.AppConstants.ApplicationName;
            Akavache.BlobCache.EnsureInitialized();

            // override the service locator to use a different set of JSON settings
            //Splat.Locator.CurrentMutable.Register(
            //    () => new Newtonsoft.Json.JsonSerializerSettings()
            //    {
            //        // these are the defaults configured for Splat in Akavache - I can't why we can't keep them
            //        ObjectCreationHandling = Newtonsoft.Json.ObjectCreationHandling.Replace,
            //        ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore,
            //        TypeNameHandling = Newtonsoft.Json.TypeNameHandling.Auto,
            //        NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore,
            //        Formatting = Newtonsoft.Json.Formatting.Indented
            //    },
            //    typeof(Newtonsoft.Json.JsonSerializerSettings));
        }

    }
}
