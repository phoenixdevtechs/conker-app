﻿
namespace Conker.Framework
{
    public interface IDeviceInfo
    {
        float StatusbarHeight { get; }
    }
}
