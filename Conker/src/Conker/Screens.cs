﻿namespace Conker
{
    public static class Screens
    {
        public static readonly string NavigationPage = nameof(NavigationPage);
        public static readonly string LoginPage = nameof(Views.LoginPage);
        public static readonly string SignupPage = nameof(Views.SignUpPage);
        public static readonly string VerificationPage = nameof(Views.VerificationPage);
        public static readonly string PhotoChoosePage = nameof(Views.PhotoChoosePage);
        public static readonly string TeamSelectionPage = nameof(Views.TeamSelectionPage);
        public static readonly string MainTabbedPage = nameof(Views.MainTabbedPage);
        public static readonly string HomePage = nameof(Views.HomePage);
        public static readonly string LeagueTablePage = nameof(Views.LeagueTablePage);
        public static readonly string ProfilePage = nameof(Views.ProfilePage);
        public static readonly string SettingsPage = nameof(Views.SettingsPage);
        public static readonly string QuizPage = nameof(Views.QuizPage);
        public static readonly string QuestionPage = nameof(Views.QuestionPage);
        public static readonly string PostPopupPage = nameof(Views.PostPopupPage);
        public static readonly string SplashScreenPage = nameof(Views.SplashScreenPage);
        public static readonly string ForgotPasswordPage = nameof(Views.ForgotPasswordPage);
        public static readonly string ChangePasswordPage = nameof(Views.ChangePasswordPage);
    }
}
