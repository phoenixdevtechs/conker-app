﻿using System;
using Xamarin.Forms;
using XF.Material.Forms.Models;
using Conker.Extensions;
using System.Collections.Generic;

namespace Conker.Converters
{
    public class MaterialMenuItemToColorConverter : IValueConverter
    {

        #region IValueConverter implementation

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            var colors = new List<Color>();
            if (value != null)
            {
                //var dummy = value.ToType<MaterialMenuItem>();

                var items = (List<MaterialMenuItem>)value;

                foreach (var item in items)
                {
                    string errorMessage = "";
                    var color = Helpers.AppConstants.LondonUnderground.GetValueOrThrow(item.Text, errorMessage);

                    if (!errorMessage.IsNullOrEmpty())
                        colors.Add(Color.White);

                   colors.Add(Color.FromHex(color));
                }

                return colors;
            }
            return Color.White;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
