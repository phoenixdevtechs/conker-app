﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using Plugin.InputKit.Shared.Helpers;
using Xamarin.Forms;

namespace Conker.Converters
{
    /// <summary>
    /// Converts a color black or white. If value is a light color, this converter will return black or if value is a dark color, this will return white.
    /// </summary>
    public class SurfaceColorConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is Color)
            {
                return ((Color)value).ToSurfaceColor();
            }
            return value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return default;
        }
    }
}
