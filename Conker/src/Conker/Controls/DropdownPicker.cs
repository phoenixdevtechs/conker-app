﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Text;
using Xamarin.Forms;
using XF.Material.Forms.UI;

namespace Conker.Controls
{
    public class DropdownPicker : Grid
    {
        public DropdownPicker()
        {

        }
        public int CornerRadius { get; set; }
        public IList ItemsSource { get => (IList)GetValue(ItemsSourceProperty); set => SetValue(ItemsSourceProperty, value); }
        public object SelectedItem { get => GetValue(SelectedItemProperty); set => SetValue(SelectedItemProperty, value); }
        public string Text { get => GetValue(TextProperty) as string; set => SetValue(TextProperty, value); }
        public bool IsPresented { get => (bool)GetValue(IsPresentedProperty); set => SetValue(IsPresentedProperty,value); }
        public Color SelectedColor { get => (Color)Application.Current.Resources["TertiaryColor"]; }
        public Color DefaultColor { get => (Color)Application.Current.Resources["LabelSectionText"]; }
        public BindingBase ItemDisplayBinding { get; set; }

        public static BindableProperty ItemsSourceProperty = BindableProperty.Create(nameof(ItemsSource), typeof(IList), typeof(DropdownPicker), propertyChanged: ItemSourceChanged);
        public static BindableProperty SelectedItemProperty = BindableProperty.Create(nameof(SelectedItem), typeof(object), typeof(DropdownPicker), propertyChanged: SelectedItemChanged);
        public static BindableProperty IsPresentedProperty = BindableProperty.Create(nameof(IsPresented), typeof(bool), typeof(DropdownPicker), false, propertyChanged: (bo, ov, nv) => (bo as DropdownPicker).UpdatePositions());
        public static BindableProperty TextProperty = BindableProperty.Create(nameof(Text), typeof(bool), typeof(DropdownPicker), false, propertyChanged: Text_Changed);




        public static void ItemSourceChanged(BindableObject bindable, object oldValue, object newValue)
        {
            if (newValue is INotifyCollectionChanged itemSource)
            {
                itemSource.CollectionChanged += (bindable as DropdownPicker).ItemSource_CollectionChanged;
            }

            (bindable as DropdownPicker).ResetView();
        }

        private static void SelectedItemChanged(BindableObject bindable, object oldValue, object newValue)
        {

        }
        private static void Text_Changed(BindableObject bindable, object oldValue, object newValue)
        {

        }
        public void ItemSource_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    foreach (var item in e.NewItems)
                        this.Children.Add(GetInstance(item, item.Equals(SelectedItem)));
                    break;
                case NotifyCollectionChangedAction.Remove:
                    foreach (var item in e.OldItems)
                        this.Children.RemoveAt(e.OldStartingIndex);
                    break;
                case NotifyCollectionChangedAction.Move:
                case NotifyCollectionChangedAction.Replace:
                case NotifyCollectionChangedAction.Reset:
                    ResetView();
                    break;
                default:
                    break;
            }

            UpdatePositions();
        }


        void ResetView()
        {
            this.Children.Clear();

            foreach (var item in ItemsSource)
            {
                var option = GetInstance(item, SelectedItem?.Equals(item) ?? false);
                //...
                this.Children.Add(option);
            }

            this.Children.Add(new Button { Text = Text, CornerRadius = this.CornerRadius, Command = new Command(()=> IsPresented = !IsPresented) });
        }
        void UpdatePositions()
        {
            for (var i = 0; i < this.Children.Count - 1; i++)
            {
                if (!this.IsPresented)
                    this.Children[i].TranslateTo(0, 0, 40);
                else
                    this.Children[i].TranslateTo(
                        0,
                        i == 0
                            ? this.Height
                            : this.Children[i - 1].TranslationY + this.Children[i - 1].Height + 1,
                        50);
            }
        }
        View GetInstance(object item, bool isSelected = false)
        {
            var btn = new MaterialButton
            {
                AllCaps = false,
               // FontFamily = (string)Resources["AvenirFontFamily"],
                ButtonType = MaterialButtonType.Flat,
            };
            btn.BackgroundColor = isSelected ? SelectedColor : DefaultColor;
            btn.CornerRadius = this.CornerRadius;
            if (ItemDisplayBinding != null)
                btn.SetBinding(Button.TextColorProperty, ItemDisplayBinding);
            else
                btn.Text = item?.ToString();

            return btn;
        }
    }
}
