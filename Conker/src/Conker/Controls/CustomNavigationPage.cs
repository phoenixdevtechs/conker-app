﻿using Xamarin.Forms;

namespace Conker.Controls
{
    public class CustomNavigationPage : NavigationPage
    {
        public CustomNavigationPage()
        {

        }
        public CustomNavigationPage(Page root) : base(root)
        {
          
        }

    }
    public interface ITransparentActionBarPage
    {
        bool IsTransparentActionBar { get; }
    }
}
