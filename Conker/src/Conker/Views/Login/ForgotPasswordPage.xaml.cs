﻿using Xamarin.Forms;
using XF.Material.Forms;

namespace Conker.Views
{
    public partial class ForgotPasswordPage : ContentPage
    {
        public ForgotPasswordPage()
        {
            InitializeComponent();

            Material.PlatformConfiguration.ChangeStatusBarColor(Color.White);
        }
    }
}
