﻿using Conker.Controls;
using Xamarin.Forms.Xaml;

namespace Conker.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MainTabbedPage : CustomTabbedPage
    {
        public MainTabbedPage()
        {
            InitializeComponent();
        }
    }
}