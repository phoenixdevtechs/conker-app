﻿using System.Windows.Input;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XF.Material.Forms;

namespace Conker.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class PhotoChoosePage : ContentPage
	{
		public PhotoChoosePage ()
		{
			InitializeComponent ();

            Material.PlatformConfiguration.ChangeStatusBarColor(Color.White);
        }

    }
}