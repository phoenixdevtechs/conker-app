﻿using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XF.Material.Forms;

namespace Conker.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ChangePasswordPage : ContentPage
    {
        public ChangePasswordPage()
        {
            InitializeComponent();

            Material.PlatformConfiguration.ChangeStatusBarColor(Color.White);
        }
    }
}
