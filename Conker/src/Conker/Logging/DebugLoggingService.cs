﻿using System;
using Conker.Services.Dialog;
using Xamarin.Forms;
using XF.Material.Forms;
using Prism.Ioc;

namespace Conker.Logging
{
    public class DebugLoggingService : ILoggingService
    {
        private readonly IDialogService dialogService;
        public DebugLoggingService() => dialogService = App.Current.Container.Resolve<IDialogService>();

        public void Debug(string message)
        {
            System.Diagnostics.Debug.WriteLine(message);
        }

        public void Warning(string message)
        {
            Debug($"# {nameof(Warning)}");
            Debug(message);
        }

        public void Error(Exception exception)
        {
            Debug($"# {nameof(Error)}");
            Debug(exception.ToString());

            dialogService.HideLoading();

            XF.Material.Forms.UI.Dialogs.MaterialDialog.Instance.AlertAsync(exception.ToString(),
               "Unexpected Error",
                new XF.Material.Forms.UI.Dialogs.Configurations.MaterialAlertDialogConfiguration
                {
                     BackgroundColor = Color.White,
                     ButtonAllCaps = false,
                     ButtonFontFamily = Material.GetResource<OnPlatform<string>>("AvenirFontFamily"),
                     MessageFontFamily = Material.GetResource<OnPlatform<string>>("AvenirFontFamily"),
                     MessageTextColor = Color.FromHex("#6D6D72"),
                     TitleFontFamily = Material.GetResource<OnPlatform<string>>("AvenirFontFamily"),
                     TitleTextColor = Color.FromHex("#333333"),
                     TintColor = Color.FromHex("#0670b2"),

                 }
           );
        }
    }
}
