﻿
namespace Conker.Models
{
    public class Team
    {
        public Team(long id,int order, string color, double score, string name)
        {
            Id = id;
            Order = order;
            Color = color;
            Score = score;
            Name = name;
        }
        public long Id { get; private set; }
        public int Order { get; set; }
        public string Color { get; private set; }
        public double Score { get; private set; }
        public string Name { get; private set; }
    }
}
