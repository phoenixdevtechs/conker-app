﻿
namespace Conker.Models
{
    public class ProfileScore
    {
        public ProfileScore()
        {
        }

        public ProfileScore(double score,  int rank, string teamName, string teamColour, double maxScore =10)
        {
            Score = score;
            MaxScore = maxScore;
            TeamName = teamName;
            TeamColour = teamColour;
            Rank = rank;
        }

        public double Score { get; set; }
        public double MaxScore { get; set; }
        public string DisplayScore => $"{Score}/{MaxScore}";
        public string TeamName { get; set; }
        public string TeamColour { get; set; }
        public int Rank { get; set; }
    }
}
