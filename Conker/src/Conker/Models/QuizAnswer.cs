﻿
namespace Conker.Models
{
    public class QuizAnswer
    {
        public QuizAnswer(string question, string answer,bool correct)
        {
            Question = question;
            Answer = answer;
            Correct = correct;
        }

        public string Question { get; private set; }
        public string Answer { get; private set; }
        public bool Correct { get; private set; }
    }
}
