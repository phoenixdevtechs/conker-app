using System.Threading.Tasks;
using Conker.Extensions;
using Conker.Logging;
using Conker.Services.Dialog;
using Plugin.LocalNotifications.Abstractions;
using Prism.Navigation;
using Prism.Services;

namespace Conker.ViewModels
{
    public class SplashScreenPageViewModel : ViewModelBase
    {
        private readonly ILocalNotifications _localNotifications;


        public SplashScreenPageViewModel(INavigationService navigationService, 
            IDialogService dialogService,
            IDeviceService deviceService,
            ILoggingService loggingService,
            ILocalNotifications localNotifications)
            : base(navigationService, dialogService, deviceService, loggingService)
        {
            _localNotifications = localNotifications;

        }

        public override async void OnNavigatedTo(INavigationParameters parameters)
        {

            INavigationResult naviResult = null;

            await Task.Delay(2000);


            if (Helpers.Settings.AuthToken.IsNullOrEmpty())
            {
                naviResult = await NavigationService.NavigateAsync($"NavigationPage/{Screens.LoginPage}");
            }
            else
            {
                naviResult = await NavigationService.NavigateAsync($"app:///{Screens.MainTabbedPage}?{KnownNavigationParameters.SelectedTab}={Screens.HomePage}");
            }

            if (Helpers.AppConstants.DebugMode)
            {
                if (!naviResult.Success)
                    await _dialogService.ShowAlertAsync(naviResult.Exception?.ToString(),Resources.AppResources.AlertTitleUnexpectedError, Resources.AppResources.AlertOK);
            }

        }
    }
}