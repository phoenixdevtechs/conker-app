using System.Threading.Tasks;
using Conker.Logging;
using Conker.Resources;
using Conker.Services.Authentication.DTOs;
using Conker.Services.Common;
using Conker.Services.Dialog;
using Conker.Validation;
using Prism.Commands;
using Prism.Navigation;
using Prism.Services;
using Xamarin.Forms;
using XF.Material.Forms;

namespace Conker.ViewModels
{
    public class LoginPageViewModel : ViewModelBase
    {
        #region Fields

        private readonly IRestPoolService _restPoolService;
        #endregion

        #region Ctor
        public LoginPageViewModel(INavigationService navigationService,
                                  IDialogService dialogService,
                                  IDeviceService deviceService,
                                  ILoggingService loggingService,
                                  IRestPoolService restPoolService)
                                 : base(navigationService, dialogService, deviceService, loggingService)
        {

            _restPoolService = restPoolService;

            ValidationService = new ValidationService<LoginPageViewModel>(this, true);
            PasswordNotEmptyRule = ValidationService.Add(e => new IsNotNullOrEmptyRule(nameof(e.Password)));
            EmailNotEmptyRule = ValidationService.Add(e => new IsNotNullOrEmptyRule(nameof(e.Email)));

            //EmailRule = ValidationService.Add(e => new EmailRule(nameof(e.Email)));
            //EntryPassRule = ValidationService.Add(e => new StrongPasswordRule(nameof(e.Password)));
        }

        #endregion

        #region Commands  
        public DelegateCommand LoginCommand => new DelegateCommand(async () => await ExecuteLoginAsync());

        #endregion

        #region Vms
        private ValidationService<LoginPageViewModel> _validationService;
        public ValidationService<LoginPageViewModel> ValidationService
        {
            get => _validationService;
            set => SetProperty(ref _validationService, value);
        }

        private string _password;
        public string Password
        {
            get => _password;
            set => SetProperty(ref _password, value);
        }

        private string _email;
        public string Email
        {
            get => _email;
            set => SetProperty(ref _email, value);
        }

        #endregion

        #region Validation


        public IsNotNullOrEmptyRule PasswordNotEmptyRule { get; }
        public IsNotNullOrEmptyRule EmailNotEmptyRule { get; }
        public StrongPasswordRule EntryPassRule { get; }
        public EmailRule EmailRule { get; }

        private async Task<bool> Validate()
        {
            if (ValidationService.IsValid)
                return true;

            ValidationService.Errors.Clear();

            if (!EmailNotEmptyRule.IsValid)
            {
                ValidationService.Errors.Add($"* {EmailNotEmptyRule.ValidationMessage}");
            }

            if (!PasswordNotEmptyRule.IsValid)
            {
                ValidationService.Errors.Add($"* {PasswordNotEmptyRule.ValidationMessage}");
            }

            //if (!EmailRule.IsValid)
            //{
            //    ValidationService.Errors.Add($"* {EmailRule.ValidationMessage}");
            //}

            //if (!EntryPassRule.IsValid)
            //{
            //    ValidationService.Errors.Add($"* {EntryPassRule.ValidationMessage}");
            //}

            await _dialogService.ShowValidationMessageAsync(ValidationService.Errors, title: "Error");

            //isValid = ValidationService.Errors.Any();

            return false;
        }
        #endregion

        #region Methods

        public override void OnAppearing()
        {
            var color = (Color)Application.Current.Resources["Primary"];
            Material.PlatformConfiguration.ChangeStatusBarColor(color);
        }

        private async Task ExecuteLoginAsync()
        {
            if (!await Validate())
                return;

            if (base.IsBusy)
                return;

            var result = await base.TryExecuteWithLoadingIndicatorsAsync<LoginResponseDto>(_restPoolService
                                   .AuthenticationAPI
                                   .Login(new AuthenticationRequestDto
                                   {
                                       Username = Email,
                                       Password = Password
                                   }));

            if (result.IsError)
                return;

            var data = result.Value;
            Helpers.Settings.AuthToken = data.AccessToken;


            var resultCurrentUser = await base.TryExecuteWithLoadingIndicatorsAsync<CurrentUserResponseDto>(_restPoolService
                                .AuthenticationAPI
                                .GetCurrentUserAsync());

            if (resultCurrentUser.IsError)
                return;


            var dataCurrentUser = resultCurrentUser.Value;
            Helpers.Settings.UserId = dataCurrentUser.Id.ToString();
            Helpers.Settings.Password = Password;

            var navigationResult = await NavigationService.NavigateAsync($"app:///{Screens.MainTabbedPage}");

            if (Helpers.AppConstants.DebugMode)
            {
                if (!navigationResult.Success)
                    await base._dialogService.ShowAlertAsync(navigationResult.Exception?.ToString(), AppResources.AlertTitleUnexpectedError, AppResources.AlertOK);
            }
        }

        #endregion
    }
}
