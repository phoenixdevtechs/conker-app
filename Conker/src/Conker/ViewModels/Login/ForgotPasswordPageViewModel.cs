﻿using System;
using System.Threading.Tasks;
using Conker.Extensions;
using Conker.Logging;
using Conker.Resources;
using Conker.Services.Authentication.DTOs;
using Conker.Services.Common;
using Conker.Services.Dialog;
using Conker.Validation;
using Prism.Commands;
using Prism.Navigation;
using Prism.Services;
using Xamarin.Forms;
using XF.Material.Forms;
using XF.Material.Forms.UI.Dialogs;

namespace Conker.ViewModels
{
    public class ForgotPasswordPageViewModel : ViewModelBase
    {
        #region Fields

        private readonly IRestPoolService _restPoolService;
        #endregion

        #region Ctor
        public ForgotPasswordPageViewModel(INavigationService navigationService,
                                  IDialogService dialogService,
                                  IDeviceService deviceService,
                                  ILoggingService loggingService,
                                  IRestPoolService restPoolService)
                                 : base(navigationService, dialogService, deviceService, loggingService)
        {

            _restPoolService = restPoolService;


        }

        #endregion

        #region Commands  
        public DelegateCommand ResetPasswordCommand => new DelegateCommand(async () => await ExecuteResetPasswordAsync());

        #endregion

        #region Vms


        private string _password;
        public string Password { get => _password; set { SetProperty(ref _password, value); } }

        private string _confirmPassword;
        public string ConfirmPassword { get => _confirmPassword; set { SetProperty(ref _confirmPassword, value); } }

        private string _email;
        public string Email
        {
            get => _email;
            set => SetProperty(ref _email, value);
        }


        private string _username;
        public string Username
        {
            get => _username;
            set
            {
                SetProperty(ref _username, value);
            }
        }

        #endregion

        #region Validation


        #endregion

        #region Methods

        private async Task ExecuteResetPasswordAsync()
        {
            if (Username.IsNullOrEmpty() && Email.IsNullOrEmpty())
            {
                await MaterialDialog.Instance.AlertAsync(
                  AppResources.ForgotPasswordPageFillUsernameAndEmailMessageText,
                  AppResources.AlertTitleGeneralWarning);

                return;
            }

            if (Password.IsNullOrEmpty())
            {
                await MaterialDialog.Instance.AlertAsync(
                 AppResources.ForgotPasswordPagePasswordEmptyMessage,
                 AppResources.AlertTitleGeneralWarning);

                return;
            }

            if (ConfirmPassword.IsNullOrEmpty())
            {
                await MaterialDialog.Instance.AlertAsync(
                 AppResources.ForgotPasswordPageConfirmPasswordEmptyMessage,
                 AppResources.AlertTitleGeneralWarning);

                return;
            }

            if (Password != ConfirmPassword)
            {
                await MaterialDialog.Instance.AlertAsync(
                    AppResources.ChangePasswordConfirmPasswordAlert,
                    AppResources.AlertTitleGeneralWarning);

                return;
            }

            //var confirm = await base._dialogService.ConfirmAsync(AppResources.ForgotPasswordPageResetPasswordMessageText, AppResources.ForgotPasswordPageTitle, Resources.AppResources.ForgotPasswordPageResetPasswordConfirmText, AppResources.AlertCancel);

            //if (confirm == false)
            //    return;

            var result = await base.TryExecuteWithLoadingIndicatorsAsync(_restPoolService
                .AuthenticationAPI
                .ForgotPassword(new ForgotPasswordRequestDto
                {
                    Email = Email,
                    Password = Password,
                    Username = Username
                }));

            if (result.IsError)
                return;

            await base._dialogService.ShowAlertAsync(AppResources.ForgotPasswordPageSuccessMessage, AppResources.AlertTitleSuccess, AppResources.AlertOK);

            await base.NavigationService.GoBackAsync();
        }

        #endregion
    }
}
