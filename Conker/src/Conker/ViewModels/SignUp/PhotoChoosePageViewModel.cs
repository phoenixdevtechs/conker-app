﻿using System;
using System.Threading.Tasks;
using Conker.Extensions;
using Conker.Logging;
using Conker.Resources;
using Conker.Services.Cache;
using Conker.Services.Dialog;
using Plugin.Media.Abstractions;
using Plugin.Permissions.Abstractions;
using Prism.Commands;
using Prism.Navigation;
using Prism.Services;
using Xamarin.Forms;
using Plugin.Permissions;
using XF.Material.Forms.Models;
using MP = Plugin.Media.CrossMedia; //MP: Media Plugin
using P = Plugin.Permissions.CrossPermissions; // P: Permissions

namespace Conker.ViewModels
{
    public class PhotoChoosePageViewModel : ViewModelBase
    {

        #region Fields

        #endregion


        #region Ctor
        public PhotoChoosePageViewModel(INavigationService navigationService,
                                        IDialogService dialogService,
                                        IDeviceService deviceService,
                                        ILoggingService loggingService)
        : base(navigationService, dialogService, deviceService, loggingService)
        {

            Options = new MaterialMenuItem[]
                        {
                                new MaterialMenuItem
                                {
                                    Text = AppResources.OptionCameraRoll,
                                },
                                new MaterialMenuItem
                                {
                                    Text =  AppResources.OptionTakePhoto,
                                }
                          };
        }

        #endregion


        #region Commands  

        public DelegateCommand<object> MenuCommand => new DelegateCommand<object>((s) => this.ExecuteMenuSelected(s));

        public DelegateCommand ContinueCommand => new DelegateCommand(async () => await ExecuteContinue());

        #endregion

        #region Vms


        private MaterialMenuItem[] _options;


        public MaterialMenuItem[] Options
        {
            get => _options;
            set => SetProperty(ref _options, value);
        }


        public ImageSource Photo { get; set; }

        #endregion

        #region Methods
        async void ExecuteMenuSelected(object param)
        {
            var result = (MaterialMenuResult)param;

            if (result.IsNullOrEmpty())
                return;

            var selectedOption = Options[result.Index];

            if (selectedOption.Text == AppResources.OptionCameraRoll)
                await ChooseFromGallery();

            if (selectedOption.Text == AppResources.OptionTakePhoto)
                await PickNewPhoto();

        }

        async Task ChooseFromGallery()
        {
            if (!MP.IsSupported || !MP.Current.IsPickPhotoSupported)
            {
                await _dialogService.ShowAlertAsync(AppResources.AlertNotSupported, AppResources.AlertTitleFeatureNotAvailable, AppResources.AlertOK);
                return;
            }

            var result = await MP.Current.PickPhotoAsync(new PickMediaOptions
            {
                MaxWidthHeight = 512,
                PhotoSize = PhotoSize.MaxWidthHeight,
            });
            if (result != null && !string.IsNullOrEmpty(result.Path))
            {
                this.Photo = ImageSource.FromStream(() => result.GetStream());

                await AkavacheImpl.SaveBlobToLocalAsync(CacheKey.UserPhoto, result.GetStream());
            }
        }

        async Task PickNewPhoto()
        {
            if (!MP.IsSupported || !MP.Current.IsTakePhotoSupported)
            {
                await _dialogService.ShowAlertAsync(AppResources.AlertNotSupported, AppResources.AlertTitleFeatureNotAvailable, AppResources.AlertOK);
                return;
            }

            var status = await P.Current.CheckPermissionStatusAsync<CameraPermission>();
            if (status != PermissionStatus.Granted)
            {
                //Additionally on Android there is a situation where you may want to detect if the user has already declined the permission and you should show your own pop up:
                if (await CrossPermissions.Current.ShouldShowRequestPermissionRationaleAsync(Permission.Camera))
                {
                    await base._dialogService.ShowAlertAsync( string.Format(AppResources.PermissionRequiredFor, AppResources.PermissionCamera), AppResources.PermissionCamera,
                                                                        AppResources.AlertOK);
                }

                status = await CrossPermissions.Current.RequestPermissionAsync<CameraPermission>();
            }

            if (status == PermissionStatus.Granted)
            {
                //Query permission
                await TakePhotoLabel();

            }

            async Task TakePhotoLabel()
            {

                var photoResult = await MP.Current.TakePhotoAsync(new StoreCameraMediaOptions
                {
                    AllowCropping = true,
                    MaxWidthHeight = 512,
                    PhotoSize = PhotoSize.MaxWidthHeight,
                    SaveToAlbum = false,
                    DefaultCamera = CameraDevice.Front,
                    Name = Guid.NewGuid().ToString("N")
                });

                if (photoResult != null && !string.IsNullOrEmpty(photoResult.Path))
                {
                    this.Photo = ImageSource.FromStream(() => photoResult.GetStream());

                    await AkavacheImpl.SaveBlobToLocalAsync(CacheKey.UserPhoto, photoResult.GetStream());
                }
            };

        }

        async Task ExecuteContinue()
        {
            var naviResult = await base.NavigationService.NavigateAsync(Screens.TeamSelectionPage);

            if (Helpers.AppConstants.DebugMode)
            {
                if (!naviResult.Success)
                {
                    base.Logging.Error(naviResult.Exception);
                }
            }
        }

        #endregion

    }
}
