﻿using System;
using System.Threading.Tasks;
using Conker.Extensions;
using Conker.Logging;
using Conker.Resources;
using Conker.Services.Authentication.DTOs;
using Conker.Services.Common;
using Conker.Services.Dialog;
using Plugin.LocalNotifications.Abstractions;
using Prism.Commands;
using Prism.Navigation;
using Prism.Services;

namespace Conker.ViewModels
{
    public class VerificationPageViewModel : ViewModelBase
    {

        #region Fields

        private readonly IRestPoolService _restPoolService;
        private readonly ILocalNotifications _localNotifications;

        #endregion


        #region Ctor
        public VerificationPageViewModel(INavigationService navigationService,
                                         IDialogService dialogService,
                                         IDeviceService deviceService,
                                         ILoggingService loggingService,
                                         IRestPoolService restPoolService,
                                         ILocalNotifications localNotifications)
        : base(navigationService, dialogService, deviceService, loggingService)
        {
            _restPoolService = restPoolService;
            _localNotifications = localNotifications;
        }

        #endregion


        #region Commands  

        public DelegateCommand ContinueCommand => new DelegateCommand(async () => await ExecuteContinue());
        public DelegateCommand ReSendCommand => new DelegateCommand(async () => await ExecuteReSend());

        #endregion


        #region Vms

        private bool _isResending;
        public bool IsResending { get => _isResending; set { SetProperty(ref _isResending, value); } }
        public bool IsValidated { get; set; }

        private string _otpText;
        public string OtpText { get => _otpText; set { SetProperty(ref _otpText, value); } }

        private string Otp { get; set; }
        private string Email { get; set; }
        private string Username { get; set; }
        private string Password { get; set; }
        private string Mobile { get; set; }
        private string Firstname { get; set; }
        private string Lastname { get; set; }

        #endregion

        #region Methods

        public override void OnNavigatedTo(INavigationParameters parameters)
        {
            NavigationMode navigationMode = parameters.GetNavigationMode();
            if (navigationMode == NavigationMode.Back)
                return;

            Email = (string)parameters["email"];
            Username = (string)parameters["username"];
            Password = (string)parameters["password"];
            Mobile = (string)parameters["mobile"];
            Firstname = (string)parameters["firstname"];
            Lastname = (string)parameters["lastname"];

            ReSendCommand?.Execute();
        }

        async Task ExecuteReSend()
        {
            try
            {
                OtpText = string.Empty;

                IsResending = true;

                await Task.Delay(TimeSpan.FromSeconds(1));

                //Service Connection
                var result = await base.TryExecuteWithLoadingIndicatorsAsync<string>(_restPoolService
                                       .OneTimePasswordAPI
                                       .Generate(Email));

                if (result.IsError)
                    return;

                Otp = result.Value;

                IsResending = false;

                await Task.Run(async() =>
                {
                    _localNotifications.Show("Conker", $"Here is your OTP to register: {Otp}");

                    await Task.Delay(TimeSpan.FromSeconds(10));

                    _localNotifications.Cancel(0);
                });
            }
            finally
            {
                IsResending = false;
            }

        }
        async Task ExecuteContinue()
        {
            if (OtpText.IsNullOrEmpty())
            {
                await _dialogService.ShowAlertAsync(AppResources.ValidationVerificationCodeEmptyMessage, "", AppResources.AlertOK);
                return;
            }

            if (Otp != OtpText)
            {
                await _dialogService.ShowAlertAsync(AppResources.ValidationVerificationCode, "", AppResources.AlertOK);
                return;
            }


            if (base.IsBusy)
                return;

            base.IsBusy = true;


            var resultSignUp = await base.TryExecuteWithLoadingIndicatorsAsync<RegisterResponseDto>(_restPoolService
                                         .AuthenticationAPI
                                         .Register(new RegisterRequestDto
                                         {
                                             Email = Email,
                                             FirstName = Firstname,
                                             LastName = Lastname,
                                             Password = Password,
                                             Username = Username,
                                             PhoneNumber = Mobile
                                         }));

            if (resultSignUp.IsError)
            {
                await base.NavigationService.GoBackAsync();
                return;
            }


            var resultVerify = await base.TryExecuteWithLoadingIndicatorsAsync<Services.OTP.DTOs.VerifyOtpResponseDto>(_restPoolService
                                         .OneTimePasswordAPI
                                         .Verify(OtpText.ToInt32(), Email));


            if (resultVerify.IsError)
            {
                OtpText = string.Empty;
                return;
            }

            var response = resultVerify.Value;

            if (response.Value.Equals("Entered Otp is NOT valid. Please Retry!"))
            {
                await _dialogService.ShowAlertAsync(AppResources.ValidationVerificationCode, "", AppResources.AlertOK);
                return;
            }

            var resultToken = await base.TryExecuteWithLoadingIndicatorsAsync<LoginResponseDto>(_restPoolService
                                       .AuthenticationAPI
                                       .Login(new AuthenticationRequestDto
                                       {
                                           Password = Password,
                                           Username = Username
                                       }));

            //Saved token to use later
            Helpers.Settings.AuthToken = resultToken.Value.AccessToken;
            Helpers.Settings.Password = Password;

            var resultCurrentUser = await base.TryExecuteWithLoadingIndicatorsAsync<CurrentUserResponseDto>(_restPoolService
                                .AuthenticationAPI
                                .GetCurrentUserAsync());

            if (resultCurrentUser.IsSuccess)
            {
                var dataCurrentUser = resultCurrentUser.Value;
                Helpers.Settings.UserId = dataCurrentUser.Id.ToString();
            }

            var naviResult = await NavigationService.NavigateAsync(Screens.PhotoChoosePage);
            if (Helpers.AppConstants.DebugMode)
            {
                if (!naviResult.Success)
                    await _dialogService.ShowAlertAsync(naviResult.Exception?.ToString(), naviResult.Exception?.Message, Resources.AppResources.AlertOK);
            }
        }

        #endregion
    }
}
