using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using Conker.Extensions;
using Conker.Logging;
using Conker.Models;
using Conker.Services.Common;
using Conker.Services.Dialog;
using Conker.Services.Leaderboard.DTOs;
using Prism.Commands;
using Prism.Navigation;
using Prism.Services;
using XF.Material.Forms.Models;
using XF.Material.Forms.UI.Dialogs;

namespace Conker.ViewModels
{
    public class TeamSelectionPageViewModel : ViewModelBase
    {

        #region Fields
        private readonly IRestPoolService _restPoolService;
        #endregion

        #region Ctor
        public TeamSelectionPageViewModel(INavigationService navigationService,
                                          IDialogService dialogService,
                                          IDeviceService deviceService,
                                          ILoggingService loggingService,
                                          IRestPoolService restPoolService)
                                         : base(navigationService,
                                                dialogService,
                                                deviceService,
                                                loggingService)
        {
            _restPoolService = restPoolService;

            //GetOptions();
        }

        #endregion

        #region Commands  

        public DelegateCommand TeamSelectedCommand => new DelegateCommand(async() =>await ExecuteOnSelected());
        public DelegateCommand ContinueCommand => new DelegateCommand(async () => await ExecuteContinue());

        #endregion

        #region Vms

        private bool _isLoading;
        public bool IsLoading { get => _isLoading; set { SetProperty(ref _isLoading, value); } }


        public List<Team> Teams { get; private set; }

        private ObservableCollection<string> _teamStringList;
        public ObservableCollection<string> TeamStringList
        {
            get => _teamStringList;
            set
            {
                SetProperty(ref _teamStringList, value);
            } }

        private Team _selectedTeam;
        public Team SelectedTeam
        {
            get => _selectedTeam;
            set
            {
                SetProperty(ref _selectedTeam, value);
            }
        }

        private string _selectedTeamText;
        public string SelectedTeamText
        {
            get => _selectedTeamText;
            set
            {
                SetProperty(ref _selectedTeamText, value);
            }
        }
        #endregion

        #region Methods

        public override async void OnNavigatingTo(INavigationParameters parameters)
        {
            await GetTeams();
        }


        private async Task ExecuteOnSelected()
        {
            //Create actions
            //Show simple dialog
            var result = await MaterialDialog.Instance.SelectActionAsync(actions: TeamStringList);

            if (result < 0)
                return;

            var selectedOption = TeamStringList[result];

            var name = (string)selectedOption;

            SelectedTeamText = name;
            SelectedTeam = Teams.First(s => s.Name == name);

        }

        private async Task GetTeams()
        {
            var result = await base.TryExecuteWithLoadingIndicatorsAsync<List<AllLeaderBoardsResponseDto>>(_restPoolService
                                   .LeaderBoardAPI
                                   .GetAllLeaderBoards());

            if (result.IsError)
                return;

            var teams = result.Value;

            foreach (var item in teams)
            {
                if (!item.Color.StartsWith("#"))
                    item.Color = $"#{item.Color}";

            }

            Teams = teams.Select(s => new Team(s.Id,new Random().Next(1,20), s.Color, 0, s.Name)).ToList();

            TeamStringList = new ObservableCollection<string>(teams.Select(s => s.Name).ToList());

        }

        private async Task ExecuteContinue()
        {
            if (SelectedTeam.IsNullOrEmpty())
            {
                await _dialogService.ShowAlertAsync(Resources.AppResources.TeamSelectionValidationMessage,
                                                    Resources.AppResources.AlertTitleGeneralWarning,
                                                    Resources.AppResources.AlertOK);
                return;
            }

            var currentUser = await _restPoolService.AuthenticationAPI.GetCurrentUserAsync();

            if (currentUser.IsNullOrEmpty())
                return;

            var result = await base.TryExecuteWithLoadingIndicatorsAsync<Services.UserLeaderBoard.DTOs.UserLeaderBoardJoinResponseDto>(_restPoolService
                               .UserLeaderBoardAPI
                               .Join(new Services.UserLeaderBoard.DTOs.UserLeaderBoardJoinRequestDto
                               {
                                   LeaderBoardId = SelectedTeam.Id,
                                   UserId = currentUser.Id
                               }));

            if (result.IsError)
                return;

           await KillThemAll();
        }


        private async Task KillThemAll()
        {
            Xamarin.Essentials.SecureStorage.RemoveAll();
            Helpers.Settings.AuthToken = string.Empty;
            var navigationResult = await NavigationService.GoBackToRootAsync();

            if (Helpers.AppConstants.DebugMode)
            {
                if (!navigationResult.Success)
                    await _dialogService.ShowAlertAsync(navigationResult.Exception?.ToString(), Resources.AppResources.AlertTitleUnexpectedError, Resources.AppResources.AlertOK);

            }
        }

        #endregion
    }
}
