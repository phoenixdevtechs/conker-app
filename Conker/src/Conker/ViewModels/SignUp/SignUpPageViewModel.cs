﻿using System.Linq;
using System.Threading.Tasks;
using Conker.Logging;
using Conker.Resources;
using Conker.Services.Common;
using Conker.Services.Dialog;
using Conker.Validation;
using Plugin.LocalNotifications.Abstractions;
using Prism.Commands;
using Prism.Navigation;
using Prism.Services;

namespace Conker.ViewModels
{
    public class SignUpPageViewModel : ViewModelBase
    {
        #region Fields

        private readonly IRestPoolService _restPoolService;
        private readonly ILocalNotifications _localNotifications;
        #endregion

        #region Ctor
        public SignUpPageViewModel(INavigationService navigationService,
                                   IDialogService dialogService,
                                   IDeviceService deviceService,
                                   ILoggingService loggingService,
                                   IRestPoolService restPoolService,
                                   ILocalNotifications localNotifications)
                                 : base(navigationService, dialogService, deviceService, loggingService)
        {

            _restPoolService = restPoolService;
            _localNotifications = localNotifications;

            ValidationService = new ValidationService<SignUpPageViewModel>(this, true);

            FirstnameNotEmptyRule = ValidationService.Add(e => new IsNotNullOrEmptyRule(nameof(e.Firstname)));
            LastnameNotEmptyRule = ValidationService.Add(e => new IsNotNullOrEmptyRule(nameof(e.Lastname)));
            UsernameNotEmptyRule = ValidationService.Add(e => new IsNotNullOrEmptyRule(nameof(e.Username)));
            MobileNotEmptyRule = ValidationService.Add(e => new IsNotNullOrEmptyRule(nameof(e.Mobile)));
            EmailNotEmptyRule = ValidationService.Add(e => new IsNotNullOrEmptyRule(nameof(e.Email)));
            PasswordNotEmptyRule = ValidationService.Add(e => new IsNotNullOrEmptyRule(nameof(e.Password)));
            ConfirmPasswordNotEmptyRule = ValidationService.Add(e => new IsNotNullOrEmptyRule(nameof(e.ConfirmPassword)));
            EmailRule = ValidationService.Add(e => new EmailRule(nameof(e.Email)));
            //StrongPasswordRule = ValidationService.Add(e => new StrongPasswordRule(nameof(e.Password)));
            //StrongConfirmPasswordRule = ValidationService.Add(e => new StrongPasswordRule(nameof(e.ConfirmPassword)));
        }

        #endregion

        #region Commands  
        public DelegateCommand ContinueCommand => new DelegateCommand(async () => await ExecuteContinue());

        #endregion

        #region Vms
        private ValidationService<SignUpPageViewModel> _validationService;
        public ValidationService<SignUpPageViewModel> ValidationService
        {
            get => _validationService;
            set => SetProperty(ref _validationService, value);
        }


        private string _password;
        public string Password { get => _password; set { SetProperty(ref _password, value); } }

        private string _confirmPassword;
        public string ConfirmPassword { get => _confirmPassword; set { SetProperty(ref _confirmPassword, value); } }


        private string _firstname;
        public string Firstname
        {
            get => _firstname;
            set
            {
                SetProperty(ref _firstname, value);
            }
        }

        private string _lastname;
        public string Lastname
        {
            get => _lastname;
            set
            {
                SetProperty(ref _lastname, value);
            }
        }

        private string _username;
        public string Username
        {
            get => _username;
            set
            {
                SetProperty(ref _username, value);
            }
        }

        private string _mobile;
        public string Mobile
        {
            get => _mobile;
            set
            {
                SetProperty(ref _mobile, value);
            }
        }

        private string _email;
        public string Email
        {
            get => _email;
            set
            {
                SetProperty(ref _email, value);
            }
        }

        /// <summary>
        /// This will be set by FormView from View.
        /// </summary>
        public bool IsValidated { get; set; }

        public bool ArePasswordsMatching { get => Password == ConfirmPassword; }

        #endregion

        #region Validation
        public IsNotNullOrEmptyRule FirstnameNotEmptyRule { get; }
        public IsNotNullOrEmptyRule LastnameNotEmptyRule { get; }
        public IsNotNullOrEmptyRule UsernameNotEmptyRule { get; }
        public IsNotNullOrEmptyRule MobileNotEmptyRule { get; }
        public IsNotNullOrEmptyRule EmailNotEmptyRule { get; }
        public IsNotNullOrEmptyRule PasswordNotEmptyRule { get; }
        public IsNotNullOrEmptyRule ConfirmPasswordNotEmptyRule { get; }

        public StrongPasswordRule StrongPasswordRule { get; }
        public StrongPasswordRule StrongConfirmPasswordRule { get; }

        public EmailRule EmailRule { get; }

        private async Task<bool> Validate()
        {

            if (ValidationService.IsValid)
                return true;

            ValidationService.Errors.Clear();


            if (!FirstnameNotEmptyRule.IsValid)
            {
                ValidationService.Errors.Add($"* {FirstnameNotEmptyRule.ValidationMessage}");
            }

            if (!LastnameNotEmptyRule.IsValid)
            {
                ValidationService.Errors.Add($"* {LastnameNotEmptyRule.ValidationMessage}");
            }


            if (!UsernameNotEmptyRule.IsValid)
            {
                ValidationService.Errors.Add($"* {UsernameNotEmptyRule.ValidationMessage}");
            }

            if (!MobileNotEmptyRule.IsValid)
            {
                ValidationService.Errors.Add($"* {MobileNotEmptyRule.ValidationMessage}");
            }

            if (!EmailNotEmptyRule.IsValid)
            {
                ValidationService.Errors.Add($"* {EmailNotEmptyRule.ValidationMessage}");
            }

            if (!PasswordNotEmptyRule.IsValid)
            {
                ValidationService.Errors.Add($"* {PasswordNotEmptyRule.ValidationMessage}");
            }

            if (!ConfirmPasswordNotEmptyRule.IsValid)
            {
                ValidationService.Errors.Add($"* {ConfirmPasswordNotEmptyRule.ValidationMessage}");
            }

            //if (!StrongPasswordRule.IsValid)
            //{
            //    ValidationService.Errors.Add($"* {StrongPasswordRule.ValidationMessage}");
            //}

            //if (!EmailRule.IsValid)
            //{
            //    ValidationService.Errors.Add($"* {EmailRule.ValidationMessage}");
            //}

            var isValid = ValidationService.Errors.Any();

            if (isValid)
            {
                await base._dialogService.ShowValidationMessageAsync(ValidationService.Errors, title: "Error");

            }

            return !isValid;
        }
        #endregion

        #region Methods

        private async Task ExecuteContinue()
        {
            if (!await this.Validate())
                return;

            if (Password != ConfirmPassword)
            {
                await base._dialogService.ShowAlertAsync(AppResources.ValidationPasswordMatch, "", AppResources.AlertOK);
                return;
            }

            try
            {
                if (base.IsBusy)
                    return;

                IsBusy = true;

                var naviParam = new NavigationParameters
                {
                    { "email", Email },
                    { "username", Username },
                    { "password", Password },
                    { "mobile", Mobile },
                    { "firstname", Firstname },
                    { "lastname", Lastname }
                };

                var result = await NavigationService.NavigateAsync(Screens.VerificationPage, naviParam);

                if (Helpers.AppConstants.DebugMode)
                {
                    if (!result.Success)
                        await base._dialogService.ShowAlertAsync(result.Exception?.ToString(), result.Exception?.Message, AppResources.AlertOK);
                }
            }
            finally
            {
                IsBusy = false;
            }
        }

        #endregion

    }
}
