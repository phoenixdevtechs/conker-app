﻿using Conker.Extensions;
using Conker.Models;
using Conker.Services.Common;
using Conker.Services.UserQuiz;
using Conker.Services.UserQuiz.DTOs;
using Prism.Commands;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;

namespace Conker.ViewModels
{
    public class PostPopupPageViewModel : ViewModelBase
    {

        private readonly IRestPoolService _restPoolService;
        public PostPopupPageViewModel(IRestPoolService restPoolService,Prism.Navigation.INavigationService navigationService, Services.Dialog.IDialogService dialogService, Prism.Services.IDeviceService deviceService, Conker.Logging.ILoggingService loggingService) : base(navigationService, dialogService, deviceService, loggingService)
        {
            _restPoolService = restPoolService;
        }

        public DelegateCommand NavigateBackCommand => new DelegateCommand(async () => {

            await base.NavigationService.ClearPopupStackAsync();
        });


        private Dictionary<int, string> ResultSet { get; set; }
        private List<Question> Questions { get; set; }


        private ObservableCollection<QuizAnswer> _quizAnswers;
        public ObservableCollection<QuizAnswer> QuizAnswers
        {
            get => _quizAnswers;
            set => SetProperty(ref _quizAnswers, value);
        }


        private int _score = 0;
        public int Score
        {
            get => _score;
            set => SetProperty(ref _score, value);
        }

        private string _todayQuiz;
        public string TodayQuiz
        {
            get => _todayQuiz;
            set => SetProperty(ref _todayQuiz, value);
        }



        public override async void OnNavigatedTo(INavigationParameters parameters)
        {
            ResultSet = (Dictionary<int, string>)parameters["result"];
            Questions = (List<Question>)parameters["questions"];
            TodayQuiz = (string)parameters["todayquiz"];
            var quizId = (int)parameters["quizId"];

            try
            {
                _dialogService.ShowLoading(Resources.AppResources.WaitMessage, Acr.UserDialogs.MaskType.Black);

                var requestDto = new UserQuizQuestionRequestDto();

                requestDto.QuizId = quizId;

                foreach (var item in ResultSet)
                {
                    requestDto.Question.Add(new UserQuizQuestionDto
                    {
                        GivenAnswer = item.Value,
                        QuestionId = item.Key
                    });
                }


                var result = await base.TryExecuteWithLoadingIndicatorsAsync<FinishStartedQuizResponseDto>(_restPoolService
                                         .UserQuizAPI
                                         .FinishStartedQuiz(requestDto),async (ex)=>
                                         {

                                             base.Logging.Error(ex);

                                             return await Task.FromResult(true);
                                         });

                if (result.IsError)
                    return;

                Score = result.Value.Score;

                var summary = await base.TryExecuteWithLoadingIndicatorsAsync<UserQuizSummaryResponseDto>(_restPoolService
                                       .UserQuizAPI
                                       .UserQuizSummary(quizId));

                QuizAnswers = new ObservableCollection<QuizAnswer>();

                if (summary.IsError)
                    return;

                foreach (var item in summary.Value.UserAnswersDTOList)
                {
                    QuizAnswers.Add(new QuizAnswer(item.Question, item.CorrectAnswer,item.Correct));
                }
              
            }
            catch (Exception ex)
            {
                _dialogService.HideLoading();
                base.Logging.Error(ex);
            }
            finally
            {
                _dialogService.HideLoading();
            }
        }
    }
}
