﻿using Conker.Models;
using Conker.Services.UserQuiz.DTOs;
using Prism.Commands;
using System.Linq;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Conker.Helpers;
using Conker.Extensions;

namespace Conker.ViewModels
{
    public class QuestionPageViewModel : ViewModelBase
    {
        #region Fields

        private Countdown countdown;
        private DateTime EndDate { get; set; }
        private DateTime StartDate { get; set; }

        #endregion

        #region Ctor

        public QuestionPageViewModel(Prism.Navigation.INavigationService navigationService, Services.Dialog.IDialogService dialogService, Prism.Services.IDeviceService deviceService, Conker.Logging.ILoggingService loggingService) : base(navigationService, dialogService, deviceService, loggingService)
        {
            countdown = new Countdown();
            ResultSet = new Dictionary<int, string>();

            Progress = 10;
            Seconds = 10;
            CompletedProgress = 0;

            ClearBackgroudnColor();

            countdown.Ticked += OnCountdownTicked;
            countdown.Completed += OnCountdownCompleted;
        }

        #endregion

        #region Commands  
        public DelegateCommand CancelCommand => new DelegateCommand(async () => await base.NavigationService.GoBackAsync(useModalNavigation: true));

        public DelegateCommand<string> Answer1Command => new DelegateCommand<string>((arg) => ExecuteAnswer(nameof(Answer1),arg));
        public DelegateCommand<string> Answer2Command => new DelegateCommand<string>((arg) => ExecuteAnswer(nameof(Answer2), arg));
        public DelegateCommand<string> Answer3Command => new DelegateCommand<string>((arg) => ExecuteAnswer(nameof(Answer3), arg));

        #endregion

        #region Vms


        private int _seconds;
        public int Seconds
        {
            get => _seconds;
            set => SetProperty(ref _seconds, value);
        }


        private double _progress;
        public double Progress
        {
            get => _progress;
            set => SetProperty(ref _progress, value);
        }

        private double _completedProgress;
        public double CompletedProgress
        {
            get => _completedProgress;
            set => SetProperty(ref _completedProgress, value);
        }

        private string _todayQuiz;

        public string TodayQuiz
        {
            get => _todayQuiz;
            set => SetProperty(ref _todayQuiz, value);
        }

        private string _questionText;
        public string QuestionText
        {
            get => _questionText;
            set => SetProperty(ref _questionText, value);
        }

        private string _answer1;
        public string Answer1
        {
            get => _answer1;
            set => SetProperty(ref _answer1, value);
        }

        private string _answer2;
        public string Answer2
        {
            get => _answer2;
            set => SetProperty(ref _answer2, value);
        }

        private string _answer3;
        public string Answer3
        {
            get => _answer3;
            set => SetProperty(ref _answer3, value);
        }

        private string _answer1BackgroundColor;

        public string Answer1BackgroundColor
        {
            get => _answer1BackgroundColor;
            set => SetProperty(ref _answer1BackgroundColor, value);
        }

        private string _answer2BackgroundColor;

        public string Answer2BackgroundColor
        {
            get => _answer2BackgroundColor;
            set => SetProperty(ref _answer2BackgroundColor, value);
        }

        private string _answer3BackgroundColor;

        public string Answer3BackgroundColor
        {
            get => _answer3BackgroundColor;
            set => SetProperty(ref _answer3BackgroundColor, value);
        }


        private int CurrenQuestionId { get; set; }
        private int CurrentQuestionIndex { get; set; }
        private Dictionary<int, string> ResultSet { get; set; }
        private List<Question> Questions { get; set; }
        private int QuizId { get; set; }

        #endregion

        #region Methods

        public override async void OnNavigatedTo(INavigationParameters parameters)
        {
            NavigationMode navigationMode = parameters.GetNavigationMode();
            if (navigationMode == NavigationMode.Back)
            {
                await base.NavigationService.GoBackAsync(useModalNavigation: true);
                return;
            }

            try
            {
                QuizId = (int)parameters["id"];
                TodayQuiz = (string)parameters["todayquiz"];
                Questions = (List<Question>)parameters["questions"];

                base.IsBusy = true;

                _dialogService.ShowLoading(Resources.AppResources.WaitMessage, Acr.UserDialogs.MaskType.Black);

                await Task.Delay(TimeSpan.FromSeconds(2));

                _dialogService.HideLoading();

                base.IsBusy = false;

                CompletedProgress = 0;
                CurrentQuestionIndex = 0;

                CurrenQuestionId = Questions[CurrentQuestionIndex].Id;
                QuestionText = Questions[CurrentQuestionIndex].QuestionString;
                Answer1 = Questions[CurrentQuestionIndex].Answer1;
                Answer2 = Questions[CurrentQuestionIndex].Answer2;
                Answer3 = Questions[CurrentQuestionIndex].Answer3;

                SetCounter();
            
            }
            catch (Exception ex)
            {
                base.Logging.Error(ex);
            }
        }

        void OnCountdownTicked()
        {
            Seconds = countdown.RemainTime.Seconds;

            var totalSeconds = (EndDate - StartDate).TotalSeconds;
            var remainSeconds = countdown.RemainTime.TotalSeconds;
            Progress = remainSeconds / totalSeconds;
        }

        async void OnCountdownCompleted()
        {
            Seconds = 0;
            Progress = 0;

            await NextQuestion();
        }

        private async Task NextQuestion()
        {
            await Task.Delay(200);
            ClearBackgroudnColor();

            CurrentQuestionIndex++;
            CompletedProgress += 10;

            if (Questions.ElementAtOrDefault(CurrentQuestionIndex) == null)
            {

                this.Destroy();

                var res = await base.NavigationService.NavigateAsync(Screens.PostPopupPage, new NavigationParameters {
                    {
                        "result",ResultSet
                    },
                    {
                        "quizId",QuizId
                    },
                    {
                       "questions",Questions
                    },
                    {
                       "todayquiz",TodayQuiz
                    }
                });

                return;
            }

            CurrenQuestionId = Questions[CurrentQuestionIndex].Id;
            QuestionText = Questions[CurrentQuestionIndex].QuestionString;
            Answer1 = Questions[CurrentQuestionIndex].Answer1;
            Answer2 = Questions[CurrentQuestionIndex].Answer2;
            Answer3 = Questions[CurrentQuestionIndex].Answer3;

          
            SetCounter();

        }

        private void SetCounter()
        {
            StartDate = DateTime.Now;
            EndDate = StartDate.AddSeconds(10);

            countdown.EndDate = EndDate;
            countdown.Start();
        }

        private void ClearBackgroudnColor()
        {

            Answer1BackgroundColor = "#dddddd";
            Answer2BackgroundColor = "#dddddd";
            Answer3BackgroundColor = "#dddddd";
        }


        private async void ExecuteAnswer(string answer,string arg)
        {
            switch (answer)
            {
                case nameof(Answer1):
                    {
                        Answer1BackgroundColor = "#0670b2";
                        break;
                    }
                case nameof(Answer2):
                    {
                        Answer2BackgroundColor = "#0670b2";
                        break;
                    }
                case nameof(Answer3):
                    {
                        Answer3BackgroundColor = "#0670b2";
                        break;
                    }
            }

            if (!ResultSet.ContainsKey(CurrenQuestionId))
            {
                ResultSet.Add(CurrenQuestionId, arg);
            }

            await NextQuestion();
        }

        public override void Destroy()
        {
            countdown.Ticked -= OnCountdownTicked;
            countdown.Completed -= OnCountdownCompleted;
        }

        #endregion
    }
}
