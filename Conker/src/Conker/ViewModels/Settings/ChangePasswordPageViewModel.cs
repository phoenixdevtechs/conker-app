﻿using System;
using System.Threading.Tasks;
using Conker.Extensions;
using Conker.Logging;
using Conker.Resources;
using Conker.Services.Authentication.DTOs;
using Conker.Services.Common;
using Conker.Services.Dialog;
using Conker.Validation;
using Prism.Commands;
using Prism.Navigation;
using Prism.Services;
using Xamarin.Forms;
using XF.Material.Forms;
using XF.Material.Forms.UI;
using XF.Material.Forms.UI.Dialogs;

namespace Conker.ViewModels
{
    public class ChangePasswordPageViewModel : ViewModelBase
    {
        #region Fields

        private readonly IRestPoolService _restPoolService;
        #endregion

        #region Ctor
        public ChangePasswordPageViewModel(INavigationService navigationService,
                                  IDialogService dialogService,
                                  IDeviceService deviceService,
                                  ILoggingService loggingService,
                                  IRestPoolService restPoolService)
                                 : base(navigationService, dialogService, deviceService, loggingService)
        {

            _restPoolService = restPoolService;


        }

        #endregion

        #region Commands  
        public DelegateCommand ChangePasswordCommand => new DelegateCommand(async () => await ExecuteChangePasswordAsync());

        #endregion

        #region Vms


        private string _newPassword;
        public string NewPassword { get => _newPassword; set { SetProperty(ref _newPassword, value); } }

        private string _confirmPassword;
        public string ConfirmPassword { get => _confirmPassword; set { SetProperty(ref _confirmPassword, value); } }

      
        #endregion

        #region Validation


        #endregion

        #region Methods

        private async Task ExecuteChangePasswordAsync()
        {

            if (NewPassword.IsNullOrEmpty() || ConfirmPassword.IsNullOrEmpty())
            {
                await base._dialogService.AlertAsync(
                 AppResources.ChangePageEmptyFieldsValidationMessage,
                 AppResources.AlertTitleGeneralWarning);

                return;
            }

            if (NewPassword != ConfirmPassword)
            {
                await base._dialogService.AlertAsync(
                    AppResources.ChangePasswordConfirmPasswordAlert,
                    AppResources.AlertTitleGeneralWarning);

                return;
            }

            var oldPassword = Helpers.Settings.Password;

            var result = await base.TryExecuteWithLoadingIndicatorsAsync<object>(_restPoolService
                .AuthenticationAPI
                .ChangePassword(new PasswordChangerDto
                {
                    NewPassword = ConfirmPassword,
                    OldPassword = oldPassword
                }));

            if (result.IsSuccess)
            {
                Helpers.Settings.Password = ConfirmPassword;
            }

            await base._dialogService.AlertAsync(AppResources.ChangedPasswordResultMessage, Resources.AppResources.AlertTitleSuccess);

            //await Task.Delay(2000);

            //await KillThemAll();

        }

        private async Task KillThemAll()
        {
            Xamarin.Essentials.SecureStorage.RemoveAll();
            Helpers.Settings.AuthToken = string.Empty;
            Helpers.Settings.UserId = string.Empty;
            await NavigationService.NavigateAsync($"app:///NavigationPage/{Screens.LoginPage}");
        }

        #endregion
    }
}
