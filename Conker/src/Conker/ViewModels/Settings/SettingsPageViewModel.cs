﻿using System;
using System.IO;
using System.Threading.Tasks;
using Conker.Extensions;
using Conker.Logging;
using Conker.Resources;
using Conker.Services.Authentication.DTOs;
using Conker.Services.Cache;
using Conker.Services.Common;
using Conker.Services.Dialog;
using Plugin.Media.Abstractions;
using Plugin.Permissions;
using Plugin.Permissions.Abstractions;
using Prism.Commands;
using Prism.Navigation;
using Prism.Services;
using Xamarin.Forms;
using XF.Material.Forms;
using XF.Material.Forms.UI.Dialogs;
using MP = Plugin.Media.CrossMedia; //MP: Media Plugin
using P = Plugin.Permissions.CrossPermissions; // P: Permissions

namespace Conker.ViewModels
{
    public class SettingsPageViewModel : ViewModelBase
    {


        #region Fields
        private readonly IRestPoolService _restPoolService;
        #endregion


        #region Ctor
        public SettingsPageViewModel(INavigationService navigationService,
            IDialogService dialogService,
            IDeviceService deviceService,
            ILoggingService loggingService,
            IRestPoolService restPoolService)
            : base(navigationService, dialogService, deviceService, loggingService)
        {
            _restPoolService = restPoolService;

            LeaderboardName = "Not Found";
            LeaderboardColour = "#0670b2";
        }

        #endregion


        #region Commands  

        public DelegateCommand PhotoChooseCommand => new DelegateCommand(async () => await this.ExecutePhotoChoose());
        public DelegateCommand LogoutCommand => new DelegateCommand(async () => await ExecuteLogoutAsync());

        #endregion

        #region Vms


        private string _username;

        public string Username
        {
            get => _username; set => SetProperty(ref _username, value);
        }


        private string _email;

        public string Email
        {
            get => _email; set => SetProperty(ref _email, value);
        }


        private string _phoneNumber;

        public string PhoneNumber
        {
            get => _phoneNumber; set => SetProperty(ref _phoneNumber, value);
        }


        private string _leaderboardColour;
        public string LeaderboardColour
        {
            get => _leaderboardColour; set => SetProperty(ref _leaderboardColour, value);
        }

        private string _leaderboardName;
        public string LeaderboardName
        {
            get => _leaderboardName; set => SetProperty(ref _leaderboardName, value);
        }


        private string _password;

        public string Password
        {
            get => _password; set => SetProperty(ref _password, value);
        }

        private ImageSource _profileImage;

        public ImageSource ProfileImage
        {
            get => _profileImage; set => SetProperty(ref _profileImage, value);
        }


        #endregion

        #region Methods
        public override void OnAppearing()
        {
            //var color = (Color)Application.Current.Resources["BikingHardColor"];
            Material.PlatformConfiguration.ChangeStatusBarColor(Color.White);
        }
        public override async void OnNavigatingTo(INavigationParameters parameters)
        {


            NavigationMode navigationMode = parameters.GetNavigationMode();
            if (navigationMode == NavigationMode.Back)
                return;

            AkavacheImpl.GetAndFetchLatest("current_user",
                async () => await _restPoolService.AuthenticationAPI.GetCurrentUserAsync(), null, null).Subscribe(
                cachedThenUpdatedThought =>
                {

                    Username = cachedThenUpdatedThought.Username;
                    LeaderboardColour = cachedThenUpdatedThought.LeaderboardColour.IsNullOrEmpty()
                        ? "#0670b2"
                        : cachedThenUpdatedThought.LeaderboardColour;
                    LeaderboardName = cachedThenUpdatedThought.LeaderboardName.IsNullOrEmpty()
                        ? "Not Found"
                        : cachedThenUpdatedThought.LeaderboardName;

                    Email = cachedThenUpdatedThought.Email;
                    PhoneNumber = cachedThenUpdatedThought.PhoneNumber;
                    // Device.BeginInvokeOnMainThread(() => theStatue.Text = augusteRodin.Title);

                });

            var byteArray = await AkavacheImpl.GetAsync(CacheKey.UserPhoto);

            ProfileImage = ImageSource.FromStream((() => new MemoryStream(byteArray)));

            Password = Helpers.Settings.Password;
        }

        private async Task ExecutePhotoChoose()
        {
            //Create actions
            var actions = new string[] { AppResources.OptionCameraRoll, AppResources.OptionTakePhoto };

            //Show simple dialog
            var result = await MaterialDialog.Instance.SelectActionAsync(actions: actions);

            if (result < 0)
                return;

            var selectedOption = actions[result];

            if (selectedOption == AppResources.OptionCameraRoll)
                await ChooseFromGallery();

            if (selectedOption == AppResources.OptionTakePhoto)
                await PickNewPhoto();

        }

       

        async Task ChooseFromGallery()
        {
            if (!MP.IsSupported || !MP.Current.IsPickPhotoSupported)
            {
                await _dialogService.ShowAlertAsync(AppResources.AlertNotSupported, AppResources.AlertTitleFeatureNotAvailable, AppResources.AlertOK);
                return;
            }

            var result = await MP.Current.PickPhotoAsync(new PickMediaOptions
            {
                MaxWidthHeight = 512,
                PhotoSize = PhotoSize.MaxWidthHeight,
            });
            if (result != null && !string.IsNullOrEmpty(result.Path))
            {
                this.ProfileImage = ImageSource.FromStream(() => result.GetStream());

                await AkavacheImpl.SaveBlobToLocalAsync(CacheKey.UserPhoto, result.GetStream());
            }
        }
        async Task PickNewPhoto()
        {
            if (!MP.IsSupported || !MP.Current.IsTakePhotoSupported)
            {
                await _dialogService.ShowAlertAsync(AppResources.AlertNotSupported, AppResources.AlertTitleFeatureNotAvailable, AppResources.AlertOK);
                return;
            }


            var status = await P.Current.CheckPermissionStatusAsync<CameraPermission>();
            if (status != PermissionStatus.Granted)
            {
                //Additionally on Android there is a situation where you may want to detect if the user has already declined the permission and you should show your own pop up:
                if (await P.Current.ShouldShowRequestPermissionRationaleAsync(Permission.Camera))
                {
                    await base._dialogService.ShowAlertAsync(string.Format(AppResources.PermissionRequiredFor, AppResources.PermissionCamera), AppResources.PermissionCamera,
                                                                        AppResources.AlertOK);
                }

                status = await P.Current.RequestPermissionAsync<CameraPermission>();
            }

            if (status == PermissionStatus.Granted)
            {
                //Query permission
                await TakePhotoLabel();

            }

            async Task TakePhotoLabel()
            {

                var photoResult = await MP.Current.TakePhotoAsync(new StoreCameraMediaOptions
                {
                    AllowCropping = true,
                    MaxWidthHeight = 512,
                    PhotoSize = PhotoSize.MaxWidthHeight,
                    SaveToAlbum = false,
                    DefaultCamera = CameraDevice.Front,
                    Name = Guid.NewGuid().ToString("N")
                });

                if (photoResult != null && !string.IsNullOrEmpty(photoResult.Path))
                {
                    this.ProfileImage = ImageSource.FromStream(() => photoResult.GetStream());

                    await AkavacheImpl.SaveBlobToLocalAsync(CacheKey.UserPhoto, photoResult.GetStream());
                }
            };
        }

        private async Task KillThemAll()
        {
            Xamarin.Essentials.SecureStorage.RemoveAll();
            Helpers.Settings.AuthToken = string.Empty;
            Helpers.Settings.UserId = string.Empty;
            await NavigationService.NavigateAsync($"app:///NavigationPage/{Screens.LoginPage}");
        }

        private async Task ExecuteLogoutAsync()
        {
            await KillThemAll();
        }

        #endregion
    }
}
