using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using Conker.Logging;
using Conker.Models;
using Conker.Resources;
using Conker.Services.Analytics.DTOs;
using Conker.Services.Common;
using Conker.Services.Dialog;
using Conker.Services.Leaderboard.DTOs;
using Prism.Commands;
using Prism.Navigation;
using Prism.Services;
using Xamarin.Forms;
using XF.Material.Forms;

namespace Conker.ViewModels
{
    public class LeagueTablePageViewModel : ViewModelBase
    {
        #region Fields
        private readonly IRestPoolService _restPoolService;

        #endregion

        #region Ctor
        public LeagueTablePageViewModel(INavigationService navigationService,
            IDialogService dialogService,
            IDeviceService deviceService,
            IRestPoolService restPoolService,
            ILoggingService loggingService)
            : base(navigationService, dialogService, deviceService, loggingService)
        {
            //First time Initalization
            _restPoolService = restPoolService;

            IsActiveChanged += HandleIsActiveTrue;
            IsActiveChanged += HandleIsActiveFalse;

        }
        #endregion

        #region Commands
        public DelegateCommand RefreshCommand => new DelegateCommand(async () => await ExecuteRefreshAsync());

        #endregion

        #region Vms
        public IList<string> Options { get; } = new[] { AppResources.LeagueTableToday, AppResources.LeagueTableThisWeek };

        private bool _isRefreshing;
        public bool IsRefreshing
        {
            get => _isRefreshing;
            set => SetProperty(ref _isRefreshing, value);
        }


        private int _selectedIndex;
        public int SelectedIndex
        {
            get => _selectedIndex;
            set
            {
                SetProperty(ref _selectedIndex, value);

                //if (Items?.Count > 0 == false)
                RefreshCommand?.Execute();
            }
        }


        private Dictionary<int, IList<Team>> _rootData = new Dictionary<int, IList<Team>>() //Init empty lists
        {
            { 0, new ObservableCollection<Team>() },
            { 1, new ObservableCollection<Team>() },
        };

        public Dictionary<int, IList<Team>> RootData
        {
            get => _rootData; set { SetProperty(ref _rootData, value); }
        }


        private ObservableCollection<Team> _items;
        public ObservableCollection<Team> Items
        {
            get => _items;
            set => SetProperty(ref _items, value);
        }

        #endregion

        #region Methods     

        public override void OnAppearing()
        {
            var color = (Color)Application.Current.Resources["Primary"];
            Material.PlatformConfiguration.ChangeStatusBarColor(color);
        }


        public async Task ExecuteRefreshAsync()
        {
            await RefreshCurrentList(true);
        }

        public async Task RefreshCurrentList(bool clear = true)
        {
            switch (SelectedIndex)
            {
                case 0:
                    await GetTeamsDailyAsync(clear);
                    break;
                case 1:
                    await GetTeamsWeeklyAsync(clear);
                    break;
            }
        }
        async Task GetTeamsDailyAsync(bool clear = true)
        {
            if (clear)
                RootData[0]?.Clear();

            try
            {
                IsRefreshing = true;

                _dialogService.ShowLoading(Resources.AppResources.WaitMessage, Acr.UserDialogs.MaskType.Black);

                //Fill data from service response
                var leaderBoards = await base.TryExecuteWithLoadingIndicatorsAsync<List<AllLeaderBoardsResponseDto>>(_restPoolService
                                    .LeaderBoardAPI
                                    .GetAllLeaderBoards());

                if (leaderBoards.IsError)
                    return;


                var dailyStatistics = await base.TryExecuteWithLoadingIndicatorsAsync<List<LeaderBoardStatisticsResponseDto>>(_restPoolService
                                  .AnalyticsAPI
                                  .GetLeaderBoardDailyStatistics());

                //var dailyStatistics = Services.Analytics.FakeLeaderBoardStatisticsResponse.Fake();

                if (dailyStatistics.IsError)
                    return;


                var topLeaderBoards = dailyStatistics.Value.OrderByDescending(s => s.Score).ToList();

                int order = 1;

                foreach (var item in topLeaderBoards)
                {
                    var leaderBoard = leaderBoards.Value.First(s => s.Id == item.LeaderBoardId);

                    RootData[0]?.Add(new Team(item.LeaderBoardId, order, leaderBoard.Color, item.Score, item.LeaderBoardName));

                    order++;
                }

                Items = new ObservableCollection<Team>(RootData?[0]);

            }
            catch (Exception e)
            {
                Logging.Error(e);
            }
            finally
            {
                _dialogService.HideLoading();

                IsRefreshing = false;
            }
        }

        async Task GetTeamsWeeklyAsync(bool clear = true)
        {
            if (clear)
                RootData[1]?.Clear();

            try
            {
                IsRefreshing = true;
                _dialogService.ShowLoading(Resources.AppResources.WaitMessage, Acr.UserDialogs.MaskType.Black);

                //Fill data from service response
                var leaderBoards = await base.TryExecuteWithLoadingIndicatorsAsync<List<AllLeaderBoardsResponseDto>>(_restPoolService
                                    .LeaderBoardAPI
                                    .GetAllLeaderBoards());

                if (leaderBoards.IsError)
                    return;


                var weeklyStatistics = await base.TryExecuteWithLoadingIndicatorsAsync<List<LeaderBoardStatisticsResponseDto>>(_restPoolService
                                  .AnalyticsAPI
                                  .GetLeaderBoardWeeklyStatistics());

                //var weeklyStatistics = Services.Analytics.FakeLeaderBoardStatisticsResponse.Fake();

                if (weeklyStatistics.IsError)
                    return;

                var topLeaderBoards = weeklyStatistics.Value.OrderByDescending(s => s.Score).ToList();

                int order = 1;
                foreach (var item in topLeaderBoards)
                {
                    var leaderBoard = leaderBoards.Value.First(s => s.Id == item.LeaderBoardId);

                    RootData[1]?.Add(new Team(item.LeaderBoardId,order ,leaderBoard.Color, item.Score, item.LeaderBoardName));

                    order++;
                }

                Items = new ObservableCollection<Team>(RootData?[1]);
            }
            catch (Exception e)
            {
                Logging.Error(e);
            }
            finally
            {
                _dialogService.HideLoading();
                IsRefreshing = false;
            }
        }


        // Use if there's some code to be executed when the tab is not 
        // the active
        private void HandleIsActiveFalse(object sender, EventArgs e)
        {
            if (IsActive == true) return;
        }

        // Use if there's some code to be executed when the tab is the active tab
        private void HandleIsActiveTrue(object sender, EventArgs e)
        {
            if (IsActive == false) return;

            try
            {
                RefreshCommand?.Execute();
            }
            catch (Exception ex)
            {
                base.Logging.Error(ex);
            }
            finally
            {
                base.IsBusy = true;
            }
        }

        public override void Destroy()
        {
            IsActiveChanged -= HandleIsActiveTrue;
            IsActiveChanged -= HandleIsActiveFalse;
        }
        #endregion
    }
}
