﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Conker.Logging;
using Conker.Models;
using Conker.Resources;
using Conker.Services.Analytics.DTOs;
using Conker.Services.Cache;
using Conker.Services.Common;
using Conker.Services.Dialog;
using Conker.Services.Leaderboard.DTOs;
using Prism.Commands;
using Prism.Navigation;
using Prism.Services;
using Xamarin.Forms;
using XF.Material.Forms;

namespace Conker.ViewModels
{
    public class ProfilePageViewModel : ViewModelBase
    {
        #region Fields
        private readonly IRestPoolService _restPoolService;
        #endregion

        #region Ctor
        public ProfilePageViewModel(INavigationService navigationService,
          IDialogService dialogService,
          IDeviceService deviceService,
          IRestPoolService restPoolService,
          ILoggingService loggingService)
         : base(navigationService, dialogService, deviceService, loggingService)
        {
            _restPoolService = restPoolService;
            ProfileImage = ImageSource.FromUri(new Uri("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS3W8UNYzNPaE38vzgzX_aebexbn94j3tKC-0MbMjmE5G30TdE7"));


            IsActiveChanged += HandleIsActiveTrue;
            IsActiveChanged += HandleIsActiveFalse;

            Material.PlatformConfiguration.ChangeStatusBarColor(Color.White);

        }
        #endregion

        #region Commands
        public DelegateCommand RefreshCommand => new DelegateCommand(async () => await ExecuteRefreshAsync());
        public DelegateCommand GetUserScoresCommand => new DelegateCommand(async () => await ExecuteGetUserScoresAsync());

        #endregion

        #region Vms
        private bool _isRefreshing;
        public bool IsRefreshing
        {
            get => _isRefreshing; set => SetProperty(ref _isRefreshing, value);
        }

        private ImageSource _profileImage;

        public ImageSource ProfileImage
        {
            get => _profileImage; set => SetProperty(ref _profileImage, value);
        }

        private string _username;

        public string Username
        {
            get => _username; set => SetProperty(ref _username, value);
        }

        private string _leaderboardColour;
        public string LeaderboardColour
        {
            get => _leaderboardColour; set => SetProperty(ref _leaderboardColour, value);
        }

        private string _leaderboardName;

        public string LeaderboardName
        {
            get => _leaderboardName; set => SetProperty(ref _leaderboardName, value);
        }

        public IList<string> Options { get; } = new[] { AppResources.ProfileToday, AppResources.ProfileThisWeek };


        private int _selectedIndex;
        public int SelectedIndex
        {
            get => _selectedIndex;
            set
            {
                SetProperty(ref _selectedIndex, value);

                //if (Items?.Count > 0 == false)
                GetUserScoresCommand?.Execute();
            }
        }

        public Dictionary<int, ProfileScore> RootData { get; } = new Dictionary<int, ProfileScore>()
        {
            //Initializing of tabs
            {0 , null },
            {1 , null },
        };


        private ProfileScore _scoreContext;
        public ProfileScore ScoreContext
        {
            get => _scoreContext;
            set => SetProperty(ref _scoreContext, value);
        }

        #endregion

        #region Methods

        public override void OnAppearing()
        {
            //var color = (Color)Application.Current.Resources["BikingHardColor"];
        }

        async Task ExecuteRefreshAsync()
        {
            IsRefreshing = true;

            //var imageAsBytes = await AkavacheImpl.GetAsync(CacheKey.UserPhoto);

            //var photo= ImageSource.FromStream(() => imageAsBytes.AsMemoryStream());

            AkavacheImpl.GetAndFetchLatest("current_user",
                        async () => await _restPoolService.AuthenticationAPI.GetCurrentUserAsync(), null, null).Subscribe(
                        cachedThenUpdatedThought =>
                        {

                            Username = cachedThenUpdatedThought.Username;
                            LeaderboardName = cachedThenUpdatedThought.LeaderboardName;
                            LeaderboardColour = cachedThenUpdatedThought.LeaderboardColour;
                        });

            var byteArrayOfImage = await AkavacheImpl.GetAsync(CacheKey.UserPhoto);

            if (byteArrayOfImage != null)
                ProfileImage = ImageSource.FromStream((() => new MemoryStream(byteArrayOfImage)));

            IsRefreshing = false;
        }

        async Task ExecuteGetUserScoresAsync(bool clear = true)
        {
            switch (SelectedIndex)
            {
                case 0:
                    await GetUserDailyAsync(clear);
                    break;
                case 1:
                    await GetUserWeeklyAsync(clear);
                    break;
            }
        }

        private async Task GetUserWeeklyAsync(bool clear = true)
        {
            if (clear)
            {
                ScoreContext = null;
                RootData[1] = null;
            }

            try
            {
                IsRefreshing = true;

                _dialogService.ShowLoading(Resources.AppResources.WaitMessage, Acr.UserDialogs.MaskType.Black);

                //Fill data from service response
                var leaderBoards = await base.TryExecuteWithLoadingIndicatorsAsync<List<AllLeaderBoardsResponseDto>>(_restPoolService
                                    .LeaderBoardAPI
                                    .GetAllLeaderBoards());

                if (leaderBoards.IsError)
                    return;

                //Fill data from service response
                var weeklyScore = await base.TryExecuteWithLoadingIndicatorsAsync<SpecificUserWeeklyScoreResponseDto>(_restPoolService
                                    .AnalyticsAPI
                                    .GetSpecificUserWeeklyScore(userId: Helpers.Settings.UserId));

                if (weeklyScore.IsError)
                    return;

                var data = weeklyScore.Value;

                var leaderBoard = leaderBoards.Value.First(s => s.Name == data.LeaderboardName);

                RootData[1] = new ProfileScore(data.WeeklyScore, data.WeeklyRank, data.LeaderboardName, leaderBoard.Color);

                ScoreContext = RootData[1];
            }
            catch (Exception e)
            {
                Logging.Error(e);
            }
            finally
            {
                _dialogService.HideLoading();
                IsRefreshing = false;
            }
        }

        private async Task GetUserDailyAsync(bool clear = true)
        {
            if (clear)
            {
                ScoreContext = null;
                RootData[0] = null;
            }

            try
            {
                IsRefreshing = true;
                _dialogService.ShowLoading(Resources.AppResources.WaitMessage, Acr.UserDialogs.MaskType.Black);

                //Fill data from service response
                var leaderBoards = await base.TryExecuteWithLoadingIndicatorsAsync<List<AllLeaderBoardsResponseDto>>(_restPoolService
                                    .LeaderBoardAPI
                                    .GetAllLeaderBoards());

                if (leaderBoards.IsError)
                    return;


                //Fill data from service response
                var dailyScore = await base.TryExecuteWithLoadingIndicatorsAsync<SpecificUserDailyScoreResponseDto>(_restPoolService
                                    .AnalyticsAPI
                                    .GetSpecificUserDailyScore(userId: Helpers.Settings.UserId));

                if (dailyScore.IsError)
                    return;

                var data = dailyScore.Value;

                var leaderBoard = leaderBoards.Value.First(s => s.Name == data.LeaderboardName);

                RootData[0] = new ProfileScore(data.Score, data.LeaderboardRank, data.LeaderboardName, leaderBoard.Color, 10);

                ScoreContext = RootData[0];
            }
            catch (Exception e)
            {
                Logging.Error(e);
            }
            finally
            {
                _dialogService.HideLoading();
                IsRefreshing = false;
            }
        }


        // Use if there's some code to be executed when the tab is not 
        // the active
        private void HandleIsActiveFalse(object sender, EventArgs e)
        {
            if (IsActive == true) return;
        }

        // Use if there's some code to be executed when the tab is the active tab
        private async void HandleIsActiveTrue(object sender, EventArgs e)
        {
            if (IsActive == false) return;

            try
            {
                Material.PlatformConfiguration.ChangeStatusBarColor(Color.White);

                await ExecuteRefreshAsync();
                await GetUserDailyAsync();
            }
            catch (Exception ex)
            {
                base.Logging.Error(ex);
            }
            finally
            {
                base.IsBusy = true;
            }

        }

        public override void Destroy()
        {
            IsActiveChanged -= HandleIsActiveTrue;
            IsActiveChanged -= HandleIsActiveFalse;
        }
        #endregion
    }
}
