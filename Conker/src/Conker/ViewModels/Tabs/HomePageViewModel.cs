using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using Conker.Extensions;
using Conker.Logging;
using Conker.Models;
using Conker.Services.Analytics.DTOs;
using Conker.Services.Common;
using Conker.Services.Dialog;
using Conker.Services.Leaderboard.DTOs;
using Conker.Services.UserQuiz.DTOs;
using Prism.Commands;
using Prism.Navigation;
using Prism.Navigation.TabbedPages;
using Prism.Services;
using Xamarin.Forms;
using XF.Material.Forms;

namespace Conker.ViewModels
{
    public class HomePageViewModel : ViewModelBase
    {
        #region Fields
        private readonly IRestPoolService _restPoolService;
        #endregion

        #region Ctor
        public HomePageViewModel(INavigationService navigationService,
            IDialogService dialogService,
            IDeviceService deviceService,
            IRestPoolService restPoolService,
            ILoggingService loggingService)
            : base(navigationService, dialogService, deviceService, loggingService)
        {
            //First time Initalization
            _restPoolService = restPoolService;


            IsActiveChanged += HandleIsActiveTrue;
            IsActiveChanged += HandleIsActiveFalse;

            TodaysQuiz = Resources.AppResources.HomePageTodayQuizText;
            AnnouncementText = Resources.AppResources.HomePageAnnouncementText;
        }
        #endregion

        #region Commands
        public DelegateCommand RefreshCommand => new DelegateCommand(async () => await ExecuteRefreshAsync());
        public DelegateCommand PlayNowCommand => new DelegateCommand(async () => await base.NavigationService.SelectTabAsync($"{Screens.QuizPage}"));

        #endregion

        #region Vms

        private bool _isRefreshing;
        public bool IsRefreshing
        {
            get => _isRefreshing;
            set => SetProperty(ref _isRefreshing, value);
        }
        private bool _isQuizLoading;
        public bool IsQuizLoading
        {
            get => _isQuizLoading;
            set => SetProperty(ref _isQuizLoading, value);
        }


        private string _announcementText;
        public string AnnouncementText
        {
            get => _announcementText;
            set => SetProperty(ref _announcementText, value);
        }

        private string _announcementColor = "#333333";
        public string AnnouncementColor
        {
            get => _announcementColor;
            set => SetProperty(ref _announcementColor, value);
        }
        


        private string _todaysQuiz;
        public string TodaysQuiz
        {
            get => _todaysQuiz;
            set => SetProperty(ref _todaysQuiz, value);
        }

        private ObservableCollection<Team> _teams;
        public ObservableCollection<Team> Teams
        {
            get => _teams;
            set => SetProperty(ref _teams, value);
        }
        #endregion

        #region Methods

        public override void OnAppearing()
        {
            var color = (Color)Application.Current.Resources["Primary"];
            Material.PlatformConfiguration.ChangeStatusBarColor(color);
        }

        private async Task GetAnnouncement()
        {
            try
            {

                var result = await base.TryExecuteWithLoadingIndicatorsAsync<YesterdayWinnerResponseDto>(_restPoolService
                                       .AnalyticsAPI
                                       .GetYesterdayWinner());

                if (result.IsError)
                    return;

                AnnouncementText = $"{Resources.AppResources.HomePageAnnouncementText} {result.Value.Name}";
                AnnouncementColor = result.Value.Color;
            }
            catch (Exception e)
            {
                Logging.Error(e);
            }
            finally
            {
                IsQuizLoading = false;
            }
        }

        private async Task GetTodaysQuiz()
        {
            try
            {
                IsQuizLoading = true;

                var result = await base.TryExecuteWithLoadingIndicatorsAsync<TodayQuizResponseDto>(_restPoolService
                                       .UserQuizAPI
                                       .TodayQuiz());

                if (result.IsError)
                    return;

                TodaysQuiz = result.Value.IsNullOrEmpty() ? Resources.AppResources.HomePageTodayQuizText : result.Value.Type.Type;
            }
            catch (Exception e)
            {
                Logging.Error(e);
            }
            finally
            {
                IsQuizLoading = false;
            }
        }

        private async Task GetTeamRankings()
        {
            try
            {
                IsRefreshing = true;

                //Fill data from service response
                var leaderBoards = await base.TryExecuteWithLoadingIndicatorsAsync<List<AllLeaderBoardsResponseDto>>(_restPoolService
                                    .LeaderBoardAPI
                                    .GetAllLeaderBoards());


                if (leaderBoards.IsError)
                    return;


                var dailyStatistics = await base.TryExecuteWithLoadingIndicatorsAsync<List<LeaderBoardStatisticsResponseDto>>(_restPoolService
                                  .AnalyticsAPI
                                  .GetLeaderBoardDailyStatistics());

                //var dailyStatistics = Services.Analytics.FakeLeaderBoardDailyStatisticsResponse.Fake();

                if (dailyStatistics.IsError)
                    return;

                if (Teams == null)
                    Teams = new ObservableCollection<Team>();
                else
                    Teams.Clear();

                //Only 3 teams leaderboard
                var top3LeaderBoards = dailyStatistics.Value.OrderByDescending(s => s.Score).Take(3).ToList();

                int order = 1;
                foreach (var item in top3LeaderBoards)
                {
                    var leaderBoard = leaderBoards.Value.First(s => s.Id == item.LeaderBoardId);

                    Teams.Add(new Team(item.LeaderBoardId,order, leaderBoard.Color, item.Score, item.LeaderBoardName));

                    order++;
                }
            }
            catch (Exception e)
            {
                Logging.Error(e);
            }
            finally
            {
                IsRefreshing = false;
            }
        }
        /// <summary>
        /// Gets required datas from service
        /// </summary>
        private async Task ExecuteRefreshAsync() //This task is not async because to make parallel requests at same time via calling async voids.
        {
            //GetTodaysQuiz();
            await GetTeamRankings();
        }

        // Use if there's some code to be executed when the tab is not 
        // the active
        private void HandleIsActiveFalse(object sender, EventArgs e)
        {
            if (IsActive == true) return;
        }

        // Use if there's some code to be executed when the tab is the active tab
        private async void HandleIsActiveTrue(object sender, EventArgs e)
        {
            if (IsActive == false) return;

            await GetTodaysQuiz();
            await GetAnnouncement();
            await GetTeamRankings();
        }

        public override void Destroy()
        {
            IsActiveChanged -= HandleIsActiveTrue;
            IsActiveChanged -= HandleIsActiveFalse;
        }
        #endregion
    }
}
