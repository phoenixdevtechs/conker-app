﻿using System;
using Xamarin.Forms;
using System.Threading.Tasks;
using Conker.Logging;
using Prism.Commands;
using Prism.Navigation;
using Prism.Services;
using Conker.Services.Dialog;
using XF.Material.Forms;
using Conker.Services.Common;
using Conker.Services.UserQuiz.DTOs;
using Conker.Extensions;
using Conker.Services.Authentication.DTOs;
using Conker.Services.Leaderboard.DTOs;
using Conker.Services.Analytics.DTOs;
using System.Linq;
using System.Collections.Generic;

namespace Conker.ViewModels
{
    public class QuizPageViewModel : ViewModelBase
    {
        #region Fields
        private readonly IRestPoolService _restPoolService;
        #endregion

        #region Ctor
        public QuizPageViewModel(
            INavigationService navigationService,
            IDialogService dialogService,
            IDeviceService deviceService,
            IRestPoolService restPoolService,
            ILoggingService loggingService)
            : base(navigationService, dialogService, deviceService, loggingService)
        {
            _restPoolService = restPoolService;

            TodaysQuiz = Resources.AppResources.HomePageTodayQuizText;



            IsActiveChanged += HandleIsActiveTrue;
            IsActiveChanged += HandleIsActiveFalse;
        }
        #endregion

        #region Commands

        public DelegateCommand StartQuizCommand => new DelegateCommand(async () => await ExecuteStartQuizAsync());


        #endregion

        #region Vms
        private bool _isQuizLoading;
        public bool IsQuizLoading
        {
            get => _isQuizLoading; set => SetProperty(ref _isQuizLoading, value);
        }

        private string _todaysQuiz;

        public string TodaysQuiz
        {
            get => _todaysQuiz; set => SetProperty(ref _todaysQuiz, value);
        }

        private int _questionsNumber = 10; //TODO: Change it to get from service
        public int QuestionsNumber
        {
            get => _questionsNumber; set => SetProperty(ref _questionsNumber, value);
        }

        private int _secondsPerQuestion = 10; //TODO: Change it to get from service
        public int SecondsPerQuestion
        {
            get => _secondsPerQuestion; set => SetProperty(ref _secondsPerQuestion, value);
        }

        private string _teamName;
        public string TeamName
        {
            get => _teamName; set => SetProperty(ref _teamName, value);
        }

        private string _teamColour;

        public string TeamColour
        {
            get => _teamColour; set => SetProperty(ref _teamColour, value);
        }

        private int _rank;
        public int Rank
        {
            get => _rank; set => SetProperty(ref _rank, value);
        }
        private double _score;
        public double Score
        {
            get => _score; set => SetProperty(ref _score, value);
        }
        #endregion

        #region Methods

        public override void OnAppearing()
        {
            var color = (Color)Application.Current.Resources["Primary"];
            Material.PlatformConfiguration.ChangeStatusBarColor(color);
        }

        private async Task GetTodaysQuiz()
        {
            try
            {
                IsQuizLoading = true;

                var result = await base.TryExecuteWithLoadingIndicatorsAsync<TodayQuizResponseDto>(_restPoolService
                                       .UserQuizAPI
                                       .TodayQuiz());

                if (result.IsError)
                    return;

                TodaysQuiz = result.Value.IsNullOrEmpty() ? Resources.AppResources.HomePageTodayQuizText : result.Value.Type.Type;
            }
            catch (Exception e)
            {
                Logging.Error(e);
            }
            finally
            {
                IsQuizLoading = false;
            }
        }

        private async Task GetLeaderBoardScore()
        {
            try
            {

                IsBusy = true;

                //Fill data from service response
                var currentUser = await base.TryExecuteWithLoadingIndicatorsAsync<CurrentUserResponseDto>(_restPoolService
                               .AuthenticationAPI
                               .GetCurrentUserAsync());

                if (currentUser.IsError)
                    return;


                //Fill data from service response
                var leaderBoards = await base.TryExecuteWithLoadingIndicatorsAsync<List<AllLeaderBoardsResponseDto>>(_restPoolService
                                    .LeaderBoardAPI
                                    .GetAllLeaderBoards());

                if (leaderBoards.IsError)
                    return;


                var leaderBoard = leaderBoards.Value.FirstOrDefault(s => s.Name == currentUser.Value.LeaderboardName);
                if (leaderBoard.IsNullOrEmpty())
                    return;

                //Fill data from service response
                var dailyStatistic = await base.TryExecuteWithLoadingIndicatorsAsync<LeaderBoardStatisticsResponseDto>(_restPoolService
                                    .AnalyticsAPI
                                    .GetLeaderBoardDailyStatistics(leaderBoardId: leaderBoard.Id));

                if (dailyStatistic.IsError)
                    return;


                TeamColour = leaderBoard.Color;
                TeamName = leaderBoard.Name;
                Score = dailyStatistic.Value.Score;
                Rank = dailyStatistic.Value.Rank;
            }
            catch (Exception e)
            {
                Logging.Error(e);
            }
            finally
            {
                IsBusy = false;
            }
        }

        private async Task ExecuteStartQuizAsync()
        {

            var todayQuiz = await base.TryExecuteWithLoadingIndicatorsAsync<TodayQuizResponseDto>(_restPoolService
                          .UserQuizAPI
                          .TodayQuiz());

            if (todayQuiz.IsError)
                return;

            var startNewQuiz = await base.TryExecuteWithLoadingIndicatorsAsync<StartNewQuizResponseDto>(_restPoolService
                           .UserQuizAPI
                           .StartNewQuiz(todayQuiz.Value.Id));

            if (startNewQuiz.IsError)
                return;


            //var todayQuiz = Services.UserQuiz.FakeGetTodaysPublishedQuizResponse.Fake();
            //var startNewQuiz = Services.UserQuiz.FakeStartNewQuizResponse.Fake(todayQuiz.Value.Id);


            var naviParameters = new NavigationParameters {
                {
                    "id",todayQuiz.Value.Id
                },
                {
                    "todayquiz",todayQuiz.Value.Type.Type
                },
                {
                    "questions", startNewQuiz.Value.Questions
                },
            };

            await base.NavigationService.NavigateAsync(Screens.QuestionPage, naviParameters, useModalNavigation: true);
        }


        // Use if there's some code to be executed when the tab is not 
        // the active
        private void HandleIsActiveFalse(object sender, EventArgs e)
        {
            if (IsActive == true) return;
        }

        // Use if there's some code to be executed when the tab is the active tab
        private async void HandleIsActiveTrue(object sender, EventArgs e)
        {
            if (IsActive == false) return;

            await GetTodaysQuiz();

            await GetLeaderBoardScore();
        }

        public override void Destroy()
        {
            IsActiveChanged -= HandleIsActiveTrue;
            IsActiveChanged -= HandleIsActiveFalse;
        }
        #endregion
    }
}
