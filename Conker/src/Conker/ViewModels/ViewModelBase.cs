using System;
using System.Threading.Tasks;
using Prism.Mvvm;
using Prism;
using Prism.AppModel;
using Prism.Navigation;
using Prism.Services;
using Conker.Logging;
using Conker.Services.Dialog;
using Conker.Helpers;

namespace Conker.ViewModels
{
    public class ViewModelBase : BindableBase, IActiveAware, INavigationAware, IDestructible, IConfirmNavigation, IConfirmNavigationAsync, IApplicationLifecycleAware, IPageLifecycleAware
    {
        private bool isBusy;

        protected readonly IDialogService _dialogService;

        protected IDeviceService _deviceService { get; }

        protected INavigationService NavigationService { get; }

        protected readonly ILoggingService Logging;

        public ViewModelBase(INavigationService navigationService, IDialogService dialogService,
                             IDeviceService deviceService, ILoggingService loggingService)
        {
            if (navigationService == null)
            {
                throw new ArgumentNullException(nameof(navigationService));
            }

            if (dialogService == null)
            {
                throw new ArgumentNullException(nameof(dialogService));
            }

            if (deviceService == null)
            {
                throw new ArgumentNullException(nameof(deviceService));
            }

            if (loggingService == null)
            {
                throw new ArgumentNullException(nameof(loggingService));
            }

            _dialogService = dialogService;
            _deviceService = deviceService;
            Logging = loggingService;
            NavigationService = navigationService;
        }

        public string Title { get; set; }

        public string Subtitle { get; set; }

        public string Icon { get; set; }

        public bool IsBusy { get => isBusy; set { isBusy = value; RaisePropertyChanged(); RaisePropertyChanged(nameof(IsNotBusy)); } }

        public bool IsNotBusy { get; set; }

        public bool CanLoadMore { get; set; }

        public string Header { get; set; }

        public string Footer { get; set; }

        private void OnIsBusyChanged() => IsNotBusy = !IsBusy;

        private void OnIsNotBusyChanged() => IsBusy = !IsNotBusy;

        #region IActiveAware

        public bool IsActive { get; set; }

        public event EventHandler IsActiveChanged;

        private void OnIsActiveChanged()
        {
            IsActiveChanged?.Invoke(this, EventArgs.Empty);

            if (IsActive)
            {
                OnIsActive();
            }
            else
            {
                OnIsNotActive();
            }
        }

        protected virtual void OnIsActive() { }

        protected virtual void OnIsNotActive() { }

        #endregion IActiveAware

        #region INavigationAware

        public virtual void OnNavigatingTo(INavigationParameters parameters) { }

        public virtual void OnNavigatedTo(INavigationParameters parameters) { }

        public virtual void OnNavigatedFrom(INavigationParameters parameters) { }

        #endregion INavigationAware

        #region IDestructible

        public virtual void Destroy() { }

        #endregion IDestructible

        #region IConfirmNavigation

        public virtual bool CanNavigate(INavigationParameters parameters) => true;

        public virtual Task<bool> CanNavigateAsync(INavigationParameters parameters) =>
            Task.FromResult(CanNavigate(parameters));

        #endregion IConfirmNavigation

        #region IApplicationLifecycleAware

        public virtual void OnResume() { }

        public virtual void OnSleep() { }

        #endregion IApplicationLifecycleAware

        #region IPageLifecycleAware

        public virtual void OnAppearing() { }

        public virtual void OnDisappearing() { }

        #endregion IPageLifecycleAware

        #region TaskHelper


        protected async Task<OperationResult.Status> TryExecuteWithLoadingIndicatorsAsync(
          Task operation,
          Func<Exception, Task<bool>> onError = null) =>
          await Helpers.TaskHelper.Create()
              .WhenStarting(() => IsBusy = true)
              .WhenFinished(() => IsBusy = false)
              .TryWithErrorHandlingAsync(operation, onError);

        protected async Task<OperationResult.Result<T>> TryExecuteWithLoadingIndicatorsAsync<T>(
            Task<T> operation,
            Func<Exception, Task<bool>> onError = null) =>
            await Helpers.TaskHelper.Create()
                .WhenStarting(() => IsBusy = true)
                .WhenFinished(() => IsBusy = false)
                .TryWithErrorHandlingAsync(operation, onError);

      

        #endregion TaskHelper
    }
}