using System;
using System.Runtime.CompilerServices;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
[assembly: InternalsVisibleTo("Conker.Droid")]
[assembly: InternalsVisibleTo("Conker.iOS")]
