﻿using Plugin.LocalNotifications;
using Plugin.LocalNotifications.Abstractions;
using Prism;
using Prism.Ioc;

namespace Conker.Droid
{
  public class AndroidInitializer : IPlatformInitializer
  {
    public void RegisterTypes(IContainerRegistry containerRegistry)
    {
            // Register Any Platform Specific Implementations that you cannot 
            // access from Shared Code
            containerRegistry.RegisterSingleton<ILocalNotifications, LocalNotificationsImplementation>(); 
    }
  }
}
