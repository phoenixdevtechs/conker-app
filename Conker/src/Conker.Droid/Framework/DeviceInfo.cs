﻿using Conker.Droid.Framework;
using Xamarin.Forms;

[assembly: Dependency(typeof(DeviceInfo))]
namespace Conker.Droid.Framework
{
    public class DeviceInfo : Conker.Framework.IDeviceInfo
    {
        public float StatusbarHeight => 0;
    }
}