﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Android.Content;
using Android.Support.Design.Internal;
using Android.Support.Design.Widget;
using Android.Views;
using Conker.Controls;
using Xamarin.Forms;
using Android.App;
using Android.OS;
using Android.Runtime;
using Android.Widget;
using Xamarin.Forms.Platform.Android.AppCompat;
using View = Android.Views.View;
using Xamarin.Forms.Platform.Android;
using Android.Graphics;
using Xamarin.Forms.PlatformConfiguration.AndroidSpecific;

[assembly: ExportRenderer(typeof(CustomTabbedPage), typeof(Conker.Droid.Renderers.AndroidCustomTabbedPageRenderer))]
namespace Conker.Droid.Renderers
{
    public static class Extensions
    {
        public static List<Android.Views.View> GetViewsByType(this Android.Views.View view, Type viewType = null)
        {
            if (!(view is ViewGroup group))
                return new List<Android.Views.View>();

            var result = new List<Android.Views.View>();

            for (int i = 0; i < group.ChildCount; i++)
            {
                var child = group.GetChildAt(i);
                result.AddRange(child.GetViewsByType(viewType));

                if (viewType == null || child.GetType() == viewType)
                    result.Add(child);
            }

            return result.Distinct().ToList();
        }

        public static void FindAndRemoveById(this Android.Views.View view, int id)
        {
            var childView = view.FindViewById(id);
            ((ViewGroup)childView.Parent).RemoveView(childView);
        }
     }

    public class AndroidCustomTabbedPageRenderer : TabbedPageRenderer
    {
        private bool isShiftModeSet;

        public AndroidCustomTabbedPageRenderer(Context context) : base(context)
        {
        }
        protected override void OnLayout(bool changed, int l, int t, int r, int b)
        {
            base.OnLayout(changed, l, t, r, b);
            try
            {
                if (!isShiftModeSet)
                {
                    var children = GetAllChildViews(ViewGroup);

                    if (children.SingleOrDefault(x => x is BottomNavigationView) is BottomNavigationView bottomNav)
                    {
                        bottomNav.SetShiftMode(false, false);
                        isShiftModeSet = true;
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine($"Error setting ShiftMode: {e}");
            }
        }

        protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);

            if (e.PropertyName.Equals(CustomTabbedPage.IsHiddenProperty.PropertyName) && Element is CustomTabbedPage customTabbed)
            {
                var views = ViewGroup.GetViewsByType(typeof(BottomNavigationView));
                if (views != null && views.Any())
                {
                    var bottomView = views.FirstOrDefault() as BottomNavigationView;
                    //bottomView.Visibility = customTabbed.IsHidden ? ViewStates.Invisible : ViewStates.Visible;
                    //bottomView.TranslationY = customTabbed.IsHidden ? bottomView.TranslationY + bottomView.Height : bottomView.TranslationY - bottomView.Height;

                    //TODO: Glenn - Hmm hiding bottom tabbar leaves empty space in Android
                    if (customTabbed.IsHidden)
                        bottomView.Animate().TranslationY(bottomView.Height);
                    else
                        bottomView.Animate().TranslationY(0);
                }
            }
        }

        protected override void OnElementChanged(ElementChangedEventArgs<Xamarin.Forms.TabbedPage> e)
        {
            base.OnElementChanged(e);

            ChangeTabsFont();
        }

        private void ChangeTabsFont()
        {

            var fontFace = Typeface.CreateFromAsset(Context.Assets, "Avenir-Book.ttf");
            var  bottomNavigationView = (GetChildAt(0) as Android.Widget.RelativeLayout).GetChildAt(1) as BottomNavigationView;
            var bottomNavMenuView = bottomNavigationView.GetChildAt(0) as BottomNavigationMenuView;

            for (int j = 0; j < bottomNavMenuView.ChildCount; j++)
            {

                var item = bottomNavMenuView.GetChildAt(j) as BottomNavigationItemView;
                var itemTitle = item.GetChildAt(1);

                var smallTextView = ((TextView)((BaselineLayout)itemTitle).GetChildAt(0));
                var largeTextView = ((TextView)((BaselineLayout)itemTitle).GetChildAt(1));

                smallTextView.SetTypeface(fontFace, TypefaceStyle.Normal);
                largeTextView.SetTypeface(fontFace, TypefaceStyle.Normal);
                smallTextView.TextSize = 11;
                largeTextView.TextSize = 12;
            }
        }

        private List<View> GetAllChildViews(View view)
        {
            if (!(view is ViewGroup group))
            {
                return new List<View> { view };
            }

            var result = new List<View>();

            for (int i = 0; i < group.ChildCount; i++)
            {
                var child = group.GetChildAt(i);

                var childList = new List<View> { child };
                childList.AddRange(GetAllChildViews(child));

                result.AddRange(childList);
            }

            return result.Distinct().ToList();
        }

        private T FindChildOfType<T>(ViewGroup viewGroup) where T : Android.Views.View
        {
            if (viewGroup == null || viewGroup.ChildCount == 0) return null;

            for (var i = 0; i < viewGroup.ChildCount; i++)
            {
                var child = viewGroup.GetChildAt(i);

                var typedChild = child as T;
                if (typedChild != null) return typedChild;

                if (!(child is ViewGroup)) continue;

                var result = FindChildOfType<T>(child as ViewGroup);

                if (result != null) return result;
            }
            return null;
        }
    }
}